use std::ops::{Add};

#[derive(Debug)]
enum SnailAtom <T>{
    Node(Box<SnailAtom<T>>, Box<SnailAtom<T>>),
    Leaf(T),
}

#[derive(Debug)]
struct SnailNumber<T> {
    left: SnailAtom<T>,
    right: SnailAtom<T>,
}

use crate::SnailAtom::{Node, Leaf};

type LeafNode<T> = crate::SnailAtom<T>;

impl <T: PartialEq>PartialEq for SnailAtom<T> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Leaf(s), Leaf(o)) => s == o,
            (Node(s0, s1), Node(o0, o1)) => s0 == o0 && s1 == o1,
            (_ , _) => false,
        }
    }
}

fn add_snail<T>(left: LeafNode<T>, right: LeafNode<T>) -> LeafNode<T> {
    Node(Box::new(left), Box::new(right))
}

fn sum_snail<T: Clone + Add + Add<Output = T>>(left: LeafNode<T>, right: LeafNode<T>) -> LeafNode<T> {
    match (left, right) {
        (Leaf(x), Leaf(y)) => Leaf(x + y),
        (Leaf(x), Node(l, r)) => {Node(Box::new(sum_snail(*l, Leaf(x.clone()))), Box::new(sum_snail(Leaf(x), *r)))},
        (Node(l, r), Leaf(y)) => {Node(Box::new(sum_snail(*l, Leaf(y.clone()))), Box::new(sum_snail(Leaf(y), *r)))},
        (Node(xl, xr), Node(yl, yr)) => { Node(Box::new(sum_snail(*xl, *xr)),
                                               Box::new(sum_snail(*yl, *yr)))},
    }
}


#[cfg(test)]
mod tests {
    use super::*;
	use super::SnailAtom::{Node, Leaf};

    #[test]
    fn test_sum_snail() {
        let l1 = Leaf(1);
        let l2 = Leaf(1);
        let res = Leaf(2);
        assert_eq!(sum_snail(l1, l2), res);

        let l1 = Leaf(1);
        let n1 = Node(Box::new(Leaf(2)), Box::new(Leaf(3)));
        let res = Node(Box::new(Leaf(3)), Box::new(Leaf(4)));
        assert_eq!(sum_snail(l1, n1), res);

    }
	#[test]
	fn test_add_leaf() {
		let sn1 = Node(Box::new(Leaf(1)), Box::new(Leaf(2)));
		let test = add_snail(Leaf(1), Leaf(2));
		assert_eq!(test, sn1);

		let sn2 = Node(Box::new(Leaf(3)), Box::new(Leaf(4)));
		assert_ne!(sn1, sn2);
	}

    #[test]
    fn test_add_node() {
		let sn1 = Node(Box::new(Leaf(1)), Box::new(Leaf(2)));
		let sn2 = Node(Box::new(Leaf(3)), Box::new(Leaf(4)));
        let test = add_snail(add_snail(Leaf(1), Leaf(2)), add_snail(Leaf(3), Leaf(4)));
        assert_eq!(add_snail(sn1, sn2), test);
    }

    //#[test]
    //fn test_part1() {
        //let input = parse_input("../data/day01_example.txt");
        //let p1 = part1(&input);
        //assert_eq!(p1, 7);
    //}

    //#[test]
    //fn test_part2() {
        //let input = parse_input("../data/day01_example.txt");
        //let p2 = part2(&input);
        //assert_eq!(p2, 5);
    //}

}



//fn parse_line(line: &str) -> (bool, Vec<char>) {

    //let mut stack: Vec<char> = Vec::new();
    //let mut ilegal: Vec<char> = Vec::new();

    //for elem in line.chars() {
        //if openers.contains(&elem) {
            //stack.push(elem);
        //} else {
            //let pair = pairs[&elem];
            //if stack.ends_with(&[pair]) {
                //stack.pop();
            //} else {
                //ok = false;
                //ilegal.push(elem);
                //return (ok, ilegal);
            //}
        //}
    //}

    //(ok, stack)
//}
