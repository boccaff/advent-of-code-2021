//https://github.com/jeffomatic/adventofcode/blob/main/2021-rust/day18a/src/main.rs


#[derive(Debug, Clone)]
pub struct SnailNumber {
    pub depth: Vec<i32>,
    pub val: Vec<i32>,
}

impl PartialEq for SnailNumber {
    fn eq(&self, other: &SnailNumber) -> bool {
        self.depth == other.depth && self.val == other.val
    }
}

impl Eq for SnailNumber {}


pub fn parse_line(input: &str) -> SnailNumber {
    let mut chars = input.chars().fuse();

    let mut val: Vec<i32> = Vec::new();
    let mut depth: Vec<i32> = Vec::new();

    let mut d: i32 = 0;

    while let Some(c) = chars.next() {
        match c {
            '[' => {
                d += 1;
            },
            ',' => {},
            ']' => {
                d -= 1;
            },
            x => {
                val.push(x.to_digit(10).unwrap() as i32);
                depth.push(d);
            },
        }
    }

    SnailNumber{val, depth}

}

fn explode_at(sn: &mut SnailNumber, i: usize) {

    if i == 0_usize {
        sn.val[2_usize] += sn.val[1_usize];
        sn.val[1_usize] = 0;
        sn.val.remove(0);

        sn.depth[1_usize] -= 1;
        sn.depth.remove(0);

    } else if i == sn.val.len() - 2_usize {
        sn.val[i - 1_usize] += sn.val[i];
        sn.val[i] = 0;

        sn.depth[i] -= 1;

        sn.val.pop();
        sn.depth.pop();
    } else {
        sn.val[i - 1_usize] += sn.val[i];
        sn.val[i + 2_usize] += sn.val[i + 1_usize];
        sn.val[i] = 0;
        sn.val.remove(i + 1);

        sn.depth[i] -= 1;
        sn.depth.remove(i + 1);
    }
}

fn split_at(sn: &mut SnailNumber, i: usize) {
    let l = sn.val[i] / 2;
    let r = sn.val[i] - l;
    sn.val[i] = l;
    sn.val.insert(i + 1_usize, r);

    sn.depth[i] += 1;
    sn.depth.insert(i + 1_usize, sn.depth[i]);
}

pub fn reduce(sn: &mut SnailNumber) {
    'out: loop {
        for i in 0..sn.depth.len() {
            if sn.depth[i] > 4 {
                explode_at(sn, i);
                continue 'out;
            }
        }

        for i in 0..sn.depth.len() {
            if sn.val[i] >= 10 {
                split_at(sn, i);
                continue 'out;
            }
        }

        break;
    }
}

pub fn add_snail(sn1: &SnailNumber, sn2: &SnailNumber) -> SnailNumber {
    if sn1.depth.len() == 0_usize {
        sn2.clone()
    } else {
        let mut d = sn1.depth.iter().map(|x| *x + 1).collect::<Vec<i32>>();
        let mut v = sn1.val.clone();
        v.extend(&sn2.val);
        d.extend(sn2.depth.iter().map(|x| *x + 1));

        let mut out = SnailNumber {depth: d, val: v};
        reduce(&mut out);
        out
    }
}

pub fn parse_input(input: &str) -> SnailNumber {
    input.lines()
        .map(|l| parse_line(l))
        .fold(SnailNumber{depth: Vec::new(), val: Vec::new()},
                |acc, x| add_snail(&acc, &x))
}



pub fn magnitude(sn: &SnailNumber) -> i32 {
    let mut sn = sn.clone();
    while sn.val.len() > 1_usize {
        for i in 0..sn.val.len() - 1_usize {
            if sn.depth[i] == sn.depth[i + 1] {
                sn.val[i] = 3 * sn.val[i] + 2 * sn.val[i + 1_usize];
                sn.depth[i] = 0.max(sn.depth[i] - 1);
                sn.val.remove(i + 1);
                sn.depth.remove(i + 1);
                break;
            }
        }
    }
    sn.val.pop().unwrap()
}

pub fn part1(input: &str) -> i32 {
    let sn = parse_input(input);
    magnitude(&sn)
}

pub fn part2(input: &str) -> i32 {
    let mut res: i32 = i32::MIN;

    for (i, li) in input.lines().enumerate() {
        for (j, lj) in input.lines().enumerate() {
            if i != j {
                let sni = parse_line(li);
                let snj = parse_line(lj);
                let sn = add_snail(&sni, &snj);
                res = res.max(
                    magnitude(&add_snail(&sni, &snj))
                    ).max(
                        magnitude(&add_snail(&sni, &snj))
                        );
            }
        }
    }
    res
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_magnitude() {
        let input = parse_line("[[1,2],3]");
        let result = magnitude(&input);
        assert_eq!(result, 27);

        let input = parse_line("[[1,2],[[3,4],5]]");
        let result = magnitude(&input);
        assert_eq!(result, 143);

        let input = parse_line("[[[[1,1],[2,2]],[3,3]],[4,4]]");
        let result = magnitude(&input);
        assert_eq!(result, 445);

        let input = parse_line("[[[[3,0],[5,3]],[4,4]],[5,5]]");
        let result = magnitude(&input);
        assert_eq!(result, 791);

        let input = parse_line("[[[[5,0],[7,4]],[5,5]],[6,6]]");
        let result = magnitude(&input);
        assert_eq!(result, 1137);

        let input = parse_line("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]");
        let result = magnitude(&input);
        assert_eq!(result, 3488);

        let input = parse_line("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
        let result = magnitude(&input);
        assert_eq!(result, 1384);

        let input = parse_line("[[9,1],[1,9]]");
        let result = magnitude(&input);
        assert_eq!(result, 129);

    }

    #[test]
    fn test_parse_input() {

        let input = "[1,1]\n[2,2]\n[3,3]\n[4,4]";
        let result = parse_input(input);
        let desired = parse_line("[[[[1,1],[2,2]],[3,3]],[4,4]]");
        assert_eq!(result, desired);

        let input = "[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]";
        let result = parse_input(input);
        let desired = parse_line("[[[[3,0],[5,3]],[4,4]],[5,5]]");
        assert_eq!(result, desired);

        let input = "[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]\n[6,6]";
        let result = parse_input(input);
        let desired = parse_line("[[[[5,0],[7,4]],[5,5]],[6,6]]");
        assert_eq!(result, desired);

        let input = "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]\n[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]\n[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]\n[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]\n[7,[5,[[3,8],[1,4]]]]\n[[2,[2,2]],[8,[8,1]]]\n[2,9]\n[1,[[[9,3],9],[[9,0],[0,7]]]]\n[[[5,[7,4]],7],1]\n[[[[4,2],2],6],[8,7]]\n";
        let result = parse_input(input);
        let desired = parse_line("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]");
        assert_eq!(result, desired);

    }


    #[test]
    fn test_reduce() {

        let mut input = parse_line("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]");
        let desired = parse_line("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
        reduce(&mut input);
        assert_eq!(input, desired);

    }

    #[test]
    fn test_split() {

        let mut input = SnailNumber {
            depth: vec![4, 4, 3, 3,  4, 4, 2, 2],
            val: vec![0, 7, 4, 15, 0, 13, 1, 1],
        };

        let desired = SnailNumber {
            depth: vec![4, 4, 3, 4, 4, 4, 4, 2, 2],
            val: vec![0, 7, 4, 7, 8, 0, 13, 1, 1],
        };
        split_at(&mut input, 3_usize);
        assert_eq!(input, desired);

        let mut input = SnailNumber {
            depth: vec![4, 4, 3, 4, 4, 4, 4, 2, 2],
            val: vec![0, 7, 4, 7, 8, 0, 13, 1, 1],
        };
        let desired = SnailNumber {
            depth: vec![4, 4, 3, 4, 4, 4, 5, 5, 2, 2],
            val: vec![0, 7, 4, 7, 8, 0, 6, 7, 1, 1],
        };
        split_at(&mut input, 6_usize);
        assert_eq!(input, desired);
    }

    #[test]
    fn test_explode() {

        let mut input = parse_line("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]");
        let desired = parse_line("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]");
        explode_at(&mut input, 0_usize);
        assert_eq!(input, desired);

        let mut input = parse_line("[[[[[9,8],1],2],3],4]");
        let desired = parse_line("[[[[0,9],2],3],4]");
        explode_at(&mut input, 0_usize);
        assert_eq!(input, desired);

        let mut input = parse_line("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");
        let desired = parse_line("[[3,[2,[8,0]]],[9,[5,[7,0]]]]");
        explode_at(&mut input, 7_usize);
        assert_eq!(input, desired);

        let mut input = parse_line("[7,[6,[5,[4,[3,2]]]]]");
        let desired = parse_line("[7,[6,[5,[7,0]]]]");
        explode_at(&mut input, 4_usize);
        assert_eq!(input, desired);


        let mut input = parse_line("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]");
        let desired = parse_line("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");
        explode_at(&mut input, 3_usize);
        assert_eq!(input, desired);
        assert_eq!(input.depth, desired.depth);
        assert_eq!(input.val, desired.val);

        let mut input = parse_line("[[6,[5,[4,[3,2]]]],1]");
        let desired = parse_line("[[6,[5,[7,0]]],3]");
        explode_at(&mut input, 3_usize);
        assert_eq!(input, desired);

    }

    #[test]
    fn test_add() {
        let sn1 = SnailNumber { depth: vec![1, 2, 2] , val: vec![1, 2, 3]};
        let sn2 = SnailNumber { depth: vec![2, 1, 1] , val: vec![3, 2, 1]};

        let expected_depth = vec![2, 3, 3, 3, 2, 2];
        let expected_val = vec![1, 2, 3, 3, 2, 1];

        let SnailNumber {depth, val } = add_snail(&sn1, &sn2);
        assert_eq!(depth, expected_depth);
        assert_eq!(val, expected_val);

    }

    #[test]
    fn test_parse() {

        let SnailNumber{val, depth}= parse_line("[1,[2,3]]");
        let exp_val = vec![1, 2, 3];
        let exp_depth = vec![1, 2, 2];
        assert_eq!(val, exp_val);
        assert_eq!(depth, exp_depth);

        let SnailNumber{val, depth}= parse_line("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]");
        let exp_val = vec![4,3,4,4,7,8,4,9,1,1];
        let exp_depth = vec![5, 5, 4,3,3,5,5,4,2,2];
        assert_eq!(val, exp_val);
        assert_eq!(depth, exp_depth);

    }

    //#[test]
    //fn test_part1() {
        //let input = parse_input("../data/day01_example.txt");
        //let p1 = part1(&input);
        //assert_eq!(p1, 7);
    //}
//#[test]
    //fn test_part2() {
        //let input = parse_input("../data/day01_example.txt");
        //let p2 = part2(&input);
        //assert_eq!(p2, 5);
    //}

}
