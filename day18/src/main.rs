use std::fs;
use day18::{parse_input, part1, part2};

fn main() {

    let input = fs::read_to_string("../data/day18_input.txt").unwrap();
    let p1 = part1(&input);
    println!("Part1: {:?}", p1);

    let input = fs::read_to_string("../data/day18_input.txt").unwrap();
    let p2 = part2(&input);
    println!("Part2: {:?}", p2);

}
