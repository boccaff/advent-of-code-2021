use day20::{parse_input, part1, part2};

fn main() {

    let (image, algo) = parse_input("../data/day20_input.txt");
    let p1 = part1(&image, &algo);
    println!("Part 1: {}", p1);

    let p2 = part2(&image, &algo);
    println!("Part 2: {}", p2);
}
