use std::fs;
use std::collections::HashMap;

const NGBS: [(i32, i32);9] =  [(-1, -1), (-1, 0), (-1, 1),
                               (0, -1), (0, 0), (0, 1),
                               (1, -1), (1, 0), (1, 1)];

pub fn parse_input(filename: &str) -> (HashMap<(i32, i32), bool>, Vec<bool>) {
    let str_input = fs::read_to_string(filename)
        .unwrap();

    let (algo_input, image_input) = str_input.split_once("\n\n")
        .unwrap();

    let algo: Vec<bool> = algo_input.chars().map(|c| c == '#').collect();
    let mut image: HashMap<(i32, i32), bool> = HashMap::new();

    image_input.lines()
        .enumerate()
        .for_each(|(i, line)| {
            line.chars()
                .enumerate()
                .for_each(|(j, c)| {
image.insert((i as i32, j as i32), c == '#');
                })
        });

    (image, algo)

}

fn boolslice_to_decimal(arr: &[bool]) -> i32 {
	arr.iter().fold(0, |acc, x| if *x {(acc << 1) + 1} else {acc << 1})
}

fn get_arr(i: i32, j: i32, set: &HashMap<(i32, i32), bool>, default: bool) -> Vec<bool> {
    NGBS.iter().map(|(di, dj)|
                    if let Some(b) = set.get(&(i + di, j + dj)) {*b} else {default}
                    ).collect()
}

fn print_image(image: &HashMap<(i32, i32), bool>) {

    let (mini, maxi) = image.keys()
        .fold((i32::MAX, i32::MIN), |acc, x| (acc.0.min(x.0), acc.1.max(x.0)));

    let (minj, maxj) = image.keys()
        .fold((i32::MAX, i32::MIN), |acc, x| (acc.0.min(x.1), acc.1.max(x.1)));

    println!("");
    for i in mini..=maxi{
        for j in minj..=maxj{
            if let Some(v) = image.get(&(i, j)) {
                if *v {
                    print!("#");
                } else {
                    print!(".");
                }
            } else {
                print!("?");
            }
        }
        print!("\n");
    }
    print!("\n");
}

fn enhance(image: &HashMap<(i32, i32), bool>, algo: &[bool], default: bool) -> HashMap<(i32, i32), bool> {
    let mut enhanced: HashMap<(i32, i32), bool> = HashMap::new();

    let (mini, maxi) = image.keys()
        .fold((i32::MAX, i32::MIN), |acc, x| (acc.0.min(x.0), acc.1.max(x.0)));

    let (minj, maxj) = image.keys()
        .fold((i32::MAX, i32::MIN), |acc, x| (acc.0.min(x.1), acc.1.max(x.1)));

    for i in (mini - 2)..=(maxi + 2){
        for j in (minj - 2)..=(maxj + 2){
            let arr = get_arr(i, j, image, default);
            let k = boolslice_to_decimal(&arr);

            enhanced.insert((i, j), algo[k as usize]);

        }
    }

    enhanced

}

pub fn part1(image: &HashMap<(i32, i32), bool>, algo: &[bool]) -> i32 {
    let mut enhanced = image.clone();
    let mut default = false;
    let empty = *algo.first().unwrap();

    for i in 0..2 {
        enhanced = enhance(&enhanced, algo, default);
        default = empty & !default;
    }

    enhanced.values().filter(|b| **b).count() as i32

}

pub fn part2(image: &HashMap<(i32, i32), bool>, algo: &[bool]) -> i32 {
    let mut enhanced = image.clone();
    let mut default = false;
    let empty = *algo.first().unwrap();

    for i in 0..50 {
        enhanced = enhance(&enhanced, algo, default);
        default = empty & !default;
    }

    print_image(&enhanced);
    enhanced.values().filter(|b| **b).count() as i32
}

#[cfg(test)]

mod tests {
    use super::*;

    #[test]
    fn test_part1(){
        let (image, algo) = parse_input("../data/day20_example.txt");
        let p1 = part1(&image, &algo);
        assert_eq!(p1, 35);
    }

    #[test]
    fn test_part2(){
        let (image, algo) = parse_input("../data/day20_example.txt");
        let p2 = part2(&image, &algo);
        assert_eq!(p2, 3351);
    }

}
