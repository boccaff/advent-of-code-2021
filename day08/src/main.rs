use day08::{parse_input, part1, part2};

fn main() {
    let input = parse_input("../data/day08_input.txt");
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}
