use std::fs;

#[derive(Debug)]
pub struct Note {
    pattern: Vec<String>,
    output: Vec<String>,
}

impl Note {
    pub fn new(pattern: Vec<String>, output: Vec<String>) -> Self {
        Note { pattern, output }
    }
}

pub fn parse_input(filename: &str) -> Vec<Note> {
    fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
        .map(|x| x.to_string())
        .map(|x| parse_line(&x))
        .collect()
}

fn parse_elems(elem: &str) -> String {
    let mut parsed: Vec<char> = elem.chars().collect();
    parsed.sort();
    parsed.into_iter().collect()
}

fn parse_side(side: &str) -> Vec<String> {
    let side = side
        .trim_start()
        .trim_end()
        .split(" ")
        .map(|x| parse_elems(x))
        .collect::<Vec<String>>();
    side
}

fn parse_line(line: &str) -> Note {
    let line = line.split("|").collect::<Vec<&str>>();
    let (left, right) = (line[0], line[1]);
    let (left, right) = (parse_side(left), parse_side(right));

    Note::new(left, right)
}

pub fn part1(notes: &[Note]) -> i32 {
    notes
        .iter()
        .map(|note| {
            note.output
                .iter()
                .map(|x| match x.len() as i32 {
                    2 | 3 | 4 | 7 => 1,
                    _ => 0,
                })
                .sum::<i32>()
        })
        .sum::<i32>()
}

// based on: https://github.com/timvisee/advent-of-code-2021/blob/master/day08b/src/main.rs
// original at the end of file runs in ~ 200 ms
// this one runs in ~ 90 ms
fn translate(note: &Note) -> i32 {
    let pat1 = note
        .pattern
        .iter()
        .find(|x| x.len() == 2_usize)
        .unwrap()
        .chars()
        .collect::<Vec<char>>();
    let pat4 = note
        .pattern
        .iter()
        .find(|x| x.len() == 4_usize)
        .unwrap()
        .chars()
        .collect::<Vec<char>>();
    note.output
        .iter()
        .map(|out| {
            match (
                out.len(),
                out.chars().filter(|x| pat1.contains(&x)).count(),
                out.chars().filter(|x| pat4.contains(&x)).count(),
            ) {
                (2, _, _) => '1',
                (3, _, _) => '7',
                (4, _, _) => '4',
                (5, 1, 2) => '2',
                (5, 2, 3) => '3',
                (5, _, _) => '5', //would be (5, 1, 3)
                (6, 1, 3) => '6',
                (6, 2, 3) => '0',
                (6, _, _) => '9',
                (7, _, _) => '8',
                _ => panic!(),
            }
        })
        .map(|x| x.to_digit(10_u32).unwrap())
        .rev()
        .enumerate()
        .map(|(i, x)| x * 10_u32.pow(i as u32))
        .sum::<u32>() as i32
}

pub fn part2(notes: &[Note]) -> i32 {
    notes.iter().map(|x| translate(x)).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let example = parse_input("../data/day08_example.txt");
        assert_eq!(part1(&example), 26);
    }

    #[test]
    fn test_note01() {
        let note = parse_line("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe");
        assert_eq!(translate(&note), 8394);
    }

    #[test]
    fn test_note02() {
        let note = parse_line(
            "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc",
        );
        assert_eq!(translate(&note), 9781);
    }

    #[test]
    fn test_note03() {
        let note = parse_line(
            "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg",
        );
        assert_eq!(translate(&note), 1197);
    }

    #[test]
    fn test_note04() {
        let note = parse_line(
            "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb",
        );
        assert_eq!(translate(&note), 9361);
    }

    #[test]
    fn test_note05() {
        let note = parse_line(
            "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea",
        );
        assert_eq!(translate(&note), 4873);
    }

    #[test]
    fn test_note06() {
        let note = parse_line(
            "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb",
        );
        assert_eq!(translate(&note), 8418);
    }

    #[test]
    fn test_note07() {
        let note = parse_line(
            "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe",
        );
        assert_eq!(translate(&note), 4548);
    }

    #[test]
    fn test_note08() {
        let note = parse_line(
            "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef",
        );
        assert_eq!(translate(&note), 1625);
    }

    #[test]
    fn test_note09() {
        let note = parse_line(
            "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb",
        );
        assert_eq!(translate(&note), 8717);
    }

    #[test]
    fn test_note10() {
        let note = parse_line(
            "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce",
        );
        assert_eq!(translate(&note), 4315);
    }

    #[test]
    fn test_part2() {
        let example = parse_input("../data/day08_example.txt");
        assert_eq!(part2(&example), 61229);
    }
}

//use std::collections::{HashSet,HashMap};

//fn filter_usize(patterns: &[String], size: usize) -> HashSet<char>{
//patterns.iter().filter(|x| x.len() == size).collect::<Vec<&String>>().first().unwrap().chars().collect()
//}

//fn filter_multiple_usize(patterns: &[String], size: usize) -> Vec<HashSet<char>>{
//patterns.iter()
//.filter(|x| x.len() == size)
//.map(|x| x.chars().collect())
//.collect()
//}

//fn build_pat(key: Vec<char>) -> String {
//let mut out: Vec<char> = key.clone();
//out.sort();
//out.iter().collect::<String>()
//}

//fn translate(note: &Note) -> i32 {
//let key = decode(note.pattern.as_slice());
//note.output.iter().map(|x| key[x]).collect::<String>().parse().unwrap()
//}

//fn decode(pattern: &[String]) -> HashMap<String, char> {

//let mut map = HashMap::new();

//let l2: HashSet<char> = filter_usize(pattern, 2_usize);
//let l3: HashSet<char> = filter_usize(pattern, 3_usize);
//let l4: HashSet<char> = filter_usize(pattern, 4_usize);
//let l7: HashSet<char> = filter_usize(pattern, 7_usize);

//let achar: char = **l3.difference(&l2).into_iter().collect::<Vec<&char>>().first().unwrap();

//let ml5 = filter_multiple_usize(pattern, 5_usize);

//let pat3 = ml5.iter()
//.filter(|x| x.is_superset(&l3))
//.collect::<Vec<&HashSet<char>>>()
//.first().unwrap().clone();

//let bchar: char = *ml5.iter()
//.fold(HashSet::<char>::new(),
//|acc, x| HashSet::from_iter(acc.union(x).copied().collect::<Vec<char>>())
//)
//.difference(&pat3).copied().collect::<HashSet<char>>()
//.intersection(&l4).copied().collect::<Vec<char>>()
//.first().unwrap();

//let dchar: char = *l4.difference(&l2).filter(|x| *x != &bchar)
//.copied().collect::<Vec<char>>().first().unwrap();

//let pat5 = ml5.iter()
//.filter(|x| x.contains(&bchar))
//.collect::<Vec<&HashSet<char>>>()
//.first().unwrap().clone();

//let gchar: char = *pat5.difference(&l4)
//.filter(|x| *x != &achar)
//.copied().collect::<Vec<char>>().first().unwrap();

//let cchar: char = *l2.difference(&pat5)
//.copied().collect::<Vec<char>>().first().unwrap();

//let fchar: char = *l2.iter()
//.filter(|x| *x != &cchar)
//.copied().collect::<Vec<char>>().first().unwrap();

//let echar: char = *l7.difference(&pat3).copied().collect::<HashSet<char>>()
//.difference(&l4).copied().collect::<Vec<char>>().first().unwrap();

//let pat1 = build_pat(l2.iter().copied().collect::<Vec<char>>()); // ok
//let pat2 = build_pat(vec![achar, cchar, dchar, echar, gchar]);
//let pat3 = build_pat(pat3.into_iter().copied().collect::<Vec<char>>()); //ok
//let pat4 = build_pat(l4.iter().copied().collect::<Vec<char>>()); // ok
//let pat5 = build_pat(pat5.into_iter().copied().collect::<Vec<char>>()); //ok
//let pat6 = build_pat(vec![achar, bchar, dchar, echar, fchar, gchar]);
//let pat7 = build_pat(l3.iter().copied().collect::<Vec<char>>()); // ok
//let pat8 = build_pat(l7.iter().copied().collect::<Vec<char>>()); // ok
//let pat9 = build_pat(vec![achar, bchar, cchar, dchar, fchar, gchar]); // ok
//let pat0 = build_pat(vec![achar, bchar, cchar, echar, fchar, gchar]);

//map.insert(pat1, '1');
//map.insert(pat2, '2');
//map.insert(pat3, '3');
//map.insert(pat4, '4');
//map.insert(pat5, '5');
//map.insert(pat6, '6');
//map.insert(pat7, '7');
//map.insert(pat8, '8');
//map.insert(pat9, '9');
//map.insert(pat0, '0');

//map
//}
