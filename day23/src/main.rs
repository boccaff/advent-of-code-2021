use std::fs;
use day23::*;

fn main() {

    let input = fs::read_to_string("../data/day23_input.txt").unwrap();
    let (burrow, amphis) = parse_input(&input);
    let p1 = solve(&amphis, &burrow, 2);
    println!("Part 1: {}", p1);

    let input = fs::read_to_string("../data/day23_input2.txt").unwrap();
    let (burrow, amphis) = parse_input(&input);
    let p2 = solve(&amphis, &burrow, 4);
    println!("Part 2: {}", p2);
}
