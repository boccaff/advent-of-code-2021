use std::collections::{BinaryHeap, HashMap, HashSet};
use std::fs;

const DOORS: [i32; 4] = [3, 5, 7, 9];

#[derive(Debug, Hash, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Amphipod {
    Amber { pos: (i32, i32), moved: bool },
    Brass { pos: (i32, i32), moved: bool },
    Copper { pos: (i32, i32), moved: bool },
    Desert { pos: (i32, i32), moved: bool },
}

impl Amphipod {

    fn as_str(&self) -> &str {
        match self {
            Amphipod::Amber { pos: _, moved: _ } => "A",
            Amphipod::Brass { pos: _, moved: _ } => "B",
            Amphipod::Copper { pos: _, moved: _ } => "C",
            Amphipod::Desert { pos: _, moved: _ } => "D",
        }
    }

    fn is_in(&self, q: &(i32, i32)) -> bool {
        match self {
            Amphipod::Amber { pos, moved: _ } => pos == q,
            Amphipod::Brass { pos, moved: _ } => pos == q,
            Amphipod::Copper { pos, moved: _ } => pos == q,
            Amphipod::Desert { pos, moved: _ } => pos == q,
        }
    }

    fn moved(&self) -> bool {
        match self {
            Amphipod::Amber { pos: _, moved } => *moved,
            Amphipod::Brass { pos: _, moved } => *moved,
            Amphipod::Copper { pos: _, moved } => *moved,
            Amphipod::Desert { pos: _, moved } => *moved,
        }
    }

    fn room(&self) -> i32 {
        match self {
            Amphipod::Amber { pos: _, moved: _ } => 0,
            Amphipod::Brass { pos: _, moved: _ } => 1,
            Amphipod::Copper { pos: _, moved: _ } => 2,
            Amphipod::Desert { pos: _, moved: _ } => 3,
        }
    }

    fn room_slots(&self, depth: i32) -> Vec<(i32, i32)> {
        match self {
            Amphipod::Amber { pos: _, moved: _ } => (2..(2 + depth)).map(|i| (3, i)).collect(),
            Amphipod::Brass { pos: _, moved: _ } => (2..(2 + depth)).map(|i| (5, i)).collect(),
            Amphipod::Copper { pos: _, moved: _ } => (2..(2 + depth)).map(|i| (7, i)).collect(),
            Amphipod::Desert { pos: _, moved: _ } => (2..(2 + depth)).map(|i| (9, i)).collect(),
        }
    }

    fn pos(&self) -> &(i32, i32) {
        match self {
            Amphipod::Amber { pos, moved: _ } => pos,
            Amphipod::Brass { pos, moved: _ } => pos,
            Amphipod::Copper { pos, moved: _ } => pos,
            Amphipod::Desert { pos, moved: _ } => pos,
        }
    }

    fn move_to(&self, dest: (i32, i32)) -> (Self, i32) {
        let cost = ((self.pos().0 - dest.0).abs() + (self.pos().1 - dest.1).abs())
            * 10_i32.pow(self.room() as u32);
        match self {
            Amphipod::Amber { pos: _, moved: _ } => (
                Amphipod::Amber {
                    pos: dest,
                    moved: true,
                },
                cost,
            ),
            Amphipod::Brass { pos: _, moved: _ } => (
                Amphipod::Brass {
                    pos: dest,
                    moved: true,
                },
                cost,
            ),
            Amphipod::Copper { pos: _, moved: _ } => (
                Amphipod::Copper {
                    pos: dest,
                    moved: true,
                },
                cost,
            ),
            Amphipod::Desert { pos: _, moved: _ } => (
                Amphipod::Desert {
                    pos: dest,
                    moved: true,
                },
                cost,
            ),
        }
    }

    fn new(s: &str, pos: (i32, i32)) -> Self {
        match s {
            "A" => Amphipod::Amber { pos, moved: false },
            "B" => Amphipod::Brass { pos, moved: false },
            "C" => Amphipod::Copper { pos, moved: false },
            "D" => Amphipod::Desert { pos, moved: false },
            _ => panic!("Amphipod constructor called with wrong type: [{}]", s),
        }
    }
}

fn find_path(from: &(i32, i32), to: &(i32, i32), nodes: &[(i32, i32)]) -> Vec<(i32, i32)> {
    let mut heap = BinaryHeap::new();
    let mut dist = HashMap::new();
    let mut prev: HashMap<(i32, i32), (i32, i32)> = HashMap::new();

    dist.insert(from, 0);
    heap.push((0, from));

    while let Some((cost, node)) = heap.pop() {
        if node == to {
            let mut results: Vec<(i32, i32)> = Vec::new();
            results.push(*node);
            while results.last().unwrap() != from {
                results.push(prev[results.last().unwrap()]);
            }
            results.reverse();
            return results;
        } else {
            nodes
                .iter()
                .filter(|(i, j)| {
                    (*i == node.0 && (j - node.1).abs() == 1)
                        || (*j == node.1 && (i - node.0).abs() == 1)
                })
                .for_each(|neighbour| {
                    let alt = 1 - cost;
                    let d = dist.entry(neighbour).or_insert(i32::MAX);
                    if alt < *d {
                        *d = alt;
                        heap.push((-*d, neighbour));
                        prev.insert(*neighbour, *node);
                    }
                })
        }
    }
    panic!()
}

pub type Burrow = HashMap<((i32, i32), (i32, i32)), Vec<(i32, i32)>>;

fn build_burrow(nodes: &[(i32, i32)]) -> Burrow {

    let rooms: Vec<(i32, i32)> = nodes.iter().filter(|(_, j)| *j != 1).copied().collect();

    let hallway: Vec<(i32, i32)> = nodes
        .iter()
        .filter(|(i, j)| *j == 1 && !DOORS.contains(i))
        .copied()
        .collect();

    let mut map = HashMap::new();

    for r in rooms.iter() {
        for h in hallway.iter() {
            let path = find_path(r, h, &nodes);
            let mut rev = path.clone();
            rev.reverse();
            map.insert((*r, *h), path);
            map.insert((*h, *r), rev);
        }
    }

    map
}

fn collect_amphipods(input: &[(i32, i32, &str)]) -> Vec<Amphipod> {
    let mut results = Vec::new();

    input.iter().for_each(|(i, j, c)| match *c {
        "A" | "B" | "C" | "D" => results.push(Amphipod::new(c, (*i, *j))),
        _ => {}
    });
    results
}

pub fn parse_input(input: &str) -> (Burrow, Vec<Amphipod>) {
    let mut results = Vec::new();
    input.lines().enumerate().for_each(|(j, line)| {
        line.split_terminator("")
            .skip(1)
            .enumerate()
            .for_each(|(i, sub)| results.push((i as i32, j as i32, sub)))
    });

    let nodes: Vec<(i32, i32)> = results
        .iter()
        .filter(|(_, _, c)| "ABCD.".chars().any(|p| &p.to_string() == *c))
        .copied()
        .map(|(i, j, _)| (i, j))
        .collect();

    (build_burrow(&nodes), collect_amphipods(&results))
}

fn generate_moves(
    amphipods: &[Amphipod],
    burrow: &Burrow,
    rooms: i32,
) -> Vec<(Vec<Amphipod>, i32)> {
    let mut results = Vec::new();

    for (i, pod) in amphipods.iter().enumerate() {
        for (from, to) in burrow.keys() {
            if pod.is_in(from)
                && !amphipods
                    .iter()
                    .enumerate()
                    .filter(|(j, _)| i != *j)
                    .any(|(_, other_pod)| other_pod.is_in(to))
            {
                if !pod.moved() && (to.1 == 1) {
                    if let Some(path) = burrow.get(&(*from, *to)) {
                        if path
                            .iter()
                            .skip(1)
                            .all(|p| !amphipods.iter().any(|some_pod| some_pod.is_in(p)))
                        {
                            let mut possible_move = amphipods.to_vec();
                            let (moved_pod, cost) = pod.move_to(*to);
                            possible_move[i] = moved_pod;
                            results.push((possible_move, cost));
                        }
                    }
                } else if pod.moved() && (from.1 == 1) {
                    let desired_slots = pod.room_slots(rooms);

                    if desired_slots.contains(to) {
                        if desired_slots.iter().all(|room| {
                            amphipods.iter().all(|some_pod| {
                                !some_pod.is_in(room) || some_pod.room() == pod.room()
                            })
                        }) {
                            let dest_room = desired_slots
                                .iter()
                                .filter(|room| {
                                    amphipods.iter().all(|some_pod| !some_pod.is_in(room))
                                })
                                .copied()
                                .collect::<Vec<(i32, i32)>>();

                            if dest_room.len() > 0 && to == dest_room.last().unwrap() {
                                if let Some(path) = burrow.get(&(*from, *to)) {
                                    if path.iter().skip(1).all(|p| {
                                        !amphipods.iter().skip(1).any(|some_pod| some_pod.is_in(p))
                                    }) {
                                        let mut possible_move = amphipods.to_vec();
                                        let (moved_pod, cost) = pod.move_to(*to);
                                        possible_move[i] = moved_pod;
                                        results.push((possible_move, cost));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    results
}

fn is_end(state: &[Amphipod], rooms: i32) -> bool {
    state.iter().all(|pod| pod.room_slots(rooms).contains(pod.pos()))
}

fn print_state(state: &[Amphipod]) {
    let skeleton = r#"#############
#...........#
###.#.#.#.###
  #.#.#.#.#
  #########"#;

        for (j, line) in skeleton.lines().enumerate() {
            for (i, c) in line.split_terminator("").skip(1).enumerate() {
                match c {
                    "#" | " " => print!("{}", c),
                    "." => if state.iter().any(|pod| pod.is_in(&(i as i32, j as i32))) {
                        for pod in state {
                            if pod.is_in(&(i as i32, j as i32)) {
                                if pod.moved() {
                                    print!("\x1b[1m{}\x1b[0m", pod.as_str());
                                } else {
                                    print!("{}", pod.as_str());
                                }
                                break
                            }
                        }

                    } else {
                        print!(".")
                    },
                    _ => panic!()
                }
            }
            println!("");
        }

}



pub fn solve(start: &[Amphipod], burrow: &Burrow, rooms: i32) -> i32 {
    let mut q = BinaryHeap::new();
    let mut dist = HashMap::new();

    q.push((0, start.to_vec()));
    dist.insert(start.to_vec(), 0);

    while let Some((cost, state)) = q.pop() {
        if is_end(&state, rooms) {
            return -cost
        } else {
            let moves = generate_moves(&state, burrow, rooms);
            for (neighbor, delta) in moves {
                let alt = delta - cost;
                let d = dist.entry(neighbor.clone()).or_insert(i32::MAX);
                if alt < *d {
                    *d = alt;
                    q.push((-alt, neighbor))
                }
            }
        }
    }
    panic!("Failed to solve!")
}

#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test_example() {
        let input = fs::read_to_string("../data/day23_example.txt").unwrap();
        let (burrow, amphis) = parse_input(&input);
        let e1 = solve(&amphis, &burrow, 12521);
        assert_eq!(e1, 12521);
    }

    #[test]
    fn test_toy2() {
        let input = r#"#############
#...........#
###D#C#B#A###
  #A#B#C#D#
  #########"#;
        let (burrow, start) = parse_input(input);
        let t2 = solve(&start, &burrow, 2);
        assert_eq!(t2, 8468);
    }

    #[test]
    fn test_toy() {
        let input = r#"#############
#...........#
###B#A#C#D###
  #A#B#C#D#
  #########"#;
        let (burrow, start) = parse_input(input);
        let t1 = solve(&start, &burrow, 2);
        assert_eq!(t1, 46);
    }

    #[test]
    fn test_is_end() {
        let input = r#"#############
#...........#
###A#B#C#D###
  #A#B#C#D#
  #########"#;
        let (_, start) = parse_input(input);
        assert!(is_end(&start, 4));

        let input = r#"#############
#...........#
###D#B#C#A###
  #A#B#C#D#
  #########"#;
        let (_, start) = parse_input(input);
        assert!(!is_end(&start, 4))
    }
}
