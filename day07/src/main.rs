use std::fs;

fn main() {
    let example: Vec<i32> = parse_input("../data/day07_example.txt");
    println!("example: {:?}", example);

    let cost = brute_solution(&example);
    println!("example solution:{:?}", cost);

    let cost = brute_nonlinear_solution(&example);
    println!("nonlinear step solution:{:?}", cost);

    let input: Vec<i32> = parse_input("../data/day07_input.txt");
    let cost = brute_solution(&input);
    println!("part 1 solution:{:?}", cost);

    let cost = brute_nonlinear_solution(&input);
    println!("part2 solution:{:?}", cost);
}

fn parse_input(filename: &str) -> Vec<i32> {
    fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .split(",")
        .map(|x| x.trim().parse().unwrap())
        .collect()
}

fn sum_of_changes(v: &[i32], p: &i32) -> i32 {
    v.iter().map(|x| if x > p { x - p } else { p - x }).sum()
}

fn brute_solution(v: &[i32]) -> (i32, i32) {
    let min_input: i32 = *v.iter().min().unwrap();
    let max_input: i32 = *v.iter().max().unwrap();

    let mut cost = (min_input..max_input)
        .map(|p| (sum_of_changes(v, &p), p))
        .collect::<Vec<(i32, i32)>>();

    cost.sort_by_key(|k| k.0);

    *cost.first().unwrap()
}

fn linear_progression(a0: &i32, an: &i32) -> i32 {
    let delta = an - a0;
    (1 + delta) * delta / 2
}

fn nonlinear_change(v: &[i32], p: &i32) -> i32 {
    v.iter()
        .map(|x| {
            if x > p {
                linear_progression(p, x)
            } else {
                linear_progression(x, p)
            }
        })
        .sum()
}

fn brute_nonlinear_solution(v: &[i32]) -> (i32, i32) {
    let min_input: i32 = *v.iter().min().unwrap();
    let max_input: i32 = *v.iter().max().unwrap();

    let mut cost = (min_input..max_input)
        .map(|p| (nonlinear_change(v, &p), p))
        .collect::<Vec<(i32, i32)>>();

    cost.sort_by_key(|k| k.0);

    *cost.first().unwrap()
}
