# Advent of Code 2021

Finally took the time to join [Advent of Code](https://adventofcode.com/) properly, while also trying to learn some [Rust](https://www.rust-lang.org/).

After finishing up with a 1 week delay, and only being able to solve some puzzles because I was off work (18-22 were rough days, 23-24 took more than 2 days each), I can only recommend AoC. Sure to join next year. Can't believe that it took me so long to do this.

## Wrapping up:
  - Using AoC to learn a language was rough, but I am proud to have used `rust` to work this through. As it was my first time doing AoC, I probably should have used python. Getting used to AoC (to some degree) while learning to parse inputs in rust (coming from python) was kind too much some days.
    - Used only `std` for solving the problems (exceptions to color some terminal in the octopus problems, and trying rayon to speed up some brute force on day 24). Next time, if I use rust again, I'll surelly build some general scripts and leverage more crates.
  - After working with tabular data almost daily for the last few years, with was very nice to work with dynamic structures, specially some graphs and path-finding problems.
  - In the last days, being behind the schedule made me avoid r/adventofcode, and it was such a big loss. Being part of the community is something special in aoc. Awesome work from moderators on reddit.
  - It is so impressive to see people solving some problems in way less time than I need to read puzzle input. Learned about a whole new world.

# Day 24
 - Had some forward prunning solution that ended up needing way to much ram and ended up giving up
 - After some failed attempts at a backward solutions (finding out possible z from final one being 0), ended up looking for reviews for hints:
  - Gave up on coding, and finally understood the alu after reading these [two](https://github.com/dphilipson/advent-of-code-2021/blob/master/src/days/day24.rs) [outstanding](https://old.reddit.com/r/adventofcode/comments/rnl0vn/2021_day_24_had_me_for_a_sec/hpuvs50/).
 - There was a moment of joy after writing up the alu parser and solving correctly the three examples from the description.
  - After 24 days, I should expect that it was to easy.

# Day 23
 - Wow! Quite some days to solve this... way more than expected and over the plan of finishing it up in 2021.
  - Had first part solved for a while, but now it solves everything under 6s. I guess that with some check of explored states and some caching it can drop to ~1s. But I should start profiling the code before.
  - After finding the solution for first part and failing second, took a look in some solutions and borrowed:
   - A lot from [here](https://github.com/jeffomatic/adventofcode/blob/main/2021-rust/day23b/src/main.rs), some ideas from [here](https://github.com/RoccoDev/aoc-2021/blob/master/src/days/day23.rs) and [there](https://github.com/AxlLind/AdventOfCode2021/blob/main/src/bin/23.rs).
 - Dijkstra, Dijkstra, Dijkstra...
  - While trying to implement UCS, missed [having contains and remove](https://github.com/rust-lang/rust/issues/82001) for `BinaryHeap`. Unfortunately, that issue ended up [closed](https://github.com/rust-lang/rust/pull/82331) and the solution does not fit well my use case. I think it is the first time that I really should look outsite `std`, as I ended up [implementing](https://gitlab.com/boccaff/advent-of-code-2021/-/commit/9e83c879841ebe52691e7101c9fe7b8936ba7ede#d81b9d8ae90e161c87b93b0f9cc49a9e8f5cf6c6_211_228)the algoritm using a Heap and a Map to have the queue and contains/remove.
 - Lessons that keep coming back:
  - Inspect your intermediante states
  - Build small tests / solve simpler cases
  - Print/debug!
    - Make your print easier to understand (formating the state vectors to display the amphipods/burrow took way less time than recreating it by hand, which I did way more times than I care to admit.)
 - Logic to move back to rooms is the ugly, but this is the first one I am ok with. Maybe a little bit of [Rust for OOP](https://oribenshir.github.io/afternoon_rusting/blog/enum-and-pattern-matching-part-1) could help me here.
 - Indexing pods by coordinates in a HashMap would be my next attempt. Maybe it can solve the position checks in both arms of `generate_moves`.

# Day21
 - Wasted a lot of time after manually counting the frequency of 5's (had 1,3,5,7 for histogram)
 - There are probably some way to cache this better, and avoid the pop/push cycles
 - Not sure if using an array here was the better call. Maybe structs of u8 + usize could be better, ans surelly the code would be easier to read.

# Day20
 - After getting padding right for the example, I was unable to address it for the puzzle input and looked up on reddit for some clues, and found something [here](https://old.reddit.com/r/adventofcode/comments/rkf5ek/2021_day_20_solutions/hpk8ykf/).
 - Again I have implemented a print_<something>, that probably should be a `impl Display`
 - Also, relying to much on `HashMaps` for tracking indexes where growing a interval should be enough. Not sure about growing `Vec` to deal with the increasing image. Changing from a `HashSet ` to `HashMap` was not necessary in the end, but I did not want to check bounds + check if belong.

# Day25
 - Parsing chars to SeaCucumber: maybe time to start using "From"?

# Day22

  After missing 18 and 19 during the weekend, decided to jump on 22 to try to get ahead. Multiple issues with my solutions:
  - Plain dict counting worked for part1 and failed for part2, as expected. Changing from a dict to a set where I add/remove 3D tuples also did not work. Some way around this could be the "coordinate compression".
  - Started a solution based on set intersection and gave up, just to work on that later. After struggling a while with tracking the negative volume, looked up a bit in the subreddit, and with the help of this comment [1](https://old.reddit.com/r/adventofcode/comments/rmk2x9/2021_day_22_visualization_hint_using_squares/hpn201z/) I was able to clean up the rules for having intersections on and off (\neg A \union B was the missing link), and the idea behind this tests [2](https://old.reddit.com/r/adventofcode/comments/rmiwnb/2021_day22_part_2_java/hpmkqsh/) helped me debug some error caused by the interesection of two negative rules (even though I agree with the commenter that the 3 test should be 19). After looking some solutions, found this [one](https://github.com/JLHwung/adventOfCode/blob/main/2021/src/day22.rs), and got a better structure for the code.

# Day16

  Messing with `&str` got old soon, but I still need to wrap up my head over binary operations over accumulators. Prefered to merge strings and deal with the mess. Most of the bugs were caused by not handling the rest of some package readings that were affecting the remainder of the code.
  I had some errors while trying to test the `remainder` from some operations, which probably would saved me some hours.
  Proud for the first time implementing such algo, but definitely troubled by the code.

# Day15
  Lost a lot of time trying to brute force, which didn work even for part1 with the example. Also had some issues with initial version using list, where I ended up maximizing rist. After reading r/adventofcode, found about the BinaryHeap that I somehow missed (that even had Djikstra implemented as example of usage), and refactored for using HashSets to store a custom struct with (x,y,cost) and the BinaryHeap to store the OpenSet, getting the solution from ~ 700 ms to < 400 ms. Probably a simple HashMap with costs and a struct for the binary heap could have been simpler and maybe fast.
  Considering how bad the Manhatan distance is for a heuristic here, maybe using plain Djikstra would be faster, as we could avoid one additional HashMap and h(p) calculation.
  To get closer to the smaller times from r/aoc I would probably need to simplify the abstractions and use another crate for HashMap/Set and BinaryHeap, include de input in the binary and maybe pre-generate the input for part2. Or just learn how to instrument/benchmark the part{1,2} function calls.
  With the results from the last days, solving full AoC under 1s will require some performance review.

# Day 14
  Lost some time with `occ += 1` and a rabbit hole trying `or_insert(polymer[k])`.
  Betting on 'part 2' will iterate many times more payed off, but if he asked about the sequence produced I'd have wasted a lot of time with the current solution. Wander how to implement a similar solution but keep track of all indexes allowing to build the 'polymer representation'. Naive solution with 'work with segments and save partials to disk' could work?

# Day 12
  Working with structs and the implementation of traits.
  The first idea of tracking connections brought more problems than helped with the additional info.
  Tried to sway away from using a HashMap for tracking objects, and ended up with a ugly solution for part2.
  Most interesting solutions out there seems to use a recursion over the graph that can work on a iterator.

  Ended up refactoring for less structs and using a Vec + HashMap combination for representing paths. With all the improvements, solution went from 2.5 s to 0.3~0.5 s.
  - What I learned from others:
    - timvisee:
      - `split_once` for spliting the lines, but ended up trying to use fold to create a HashMap inside the iterator. Learned about `for_each`



# Notes

  - Trying to use only std.
    - Except on day 11 to color some animated text
    - Tried to refactor using itertools on day 09
  - Days 1 - 7 most of code can be found in `src/main.rs`
  - From day 8, after looking on some repos, settled for a pattern where:
    - `main.rs` have only a couple function calls for functions that solve part1 and part2 and print the solution to screen
    - `lib.rs` have most of the code, using the example as tests for the functions for part1 and part2
 - Some solutions on the first days received the input filename as arguments, which was later droped for hardcoded strings on `lib.rs` and `main.rs` for the examples and personal problem input. This repo assumes a `data` folder at root containing the solutions, with files named ad `day{DD}_{input,example}.txt`
  - Used cargo 1.56.0 on linux
  - After finishing the problems, I have been checking up this repos to learn how more experienced people are solving the problems. I have looked other repos and sources, but I like the coding style here better. Most of the time, I am quite surprised with the efficiency of their solutions. Some solutions even motivated me to do some refactoring.
    - https://github.com/timvisee/advent-of-code-2021
    - https://github.com/RoccoDev/aoc-2021
    - https://github.com/AxlLind/AdventOfCode2021


# What now?
  - `nom` seems to be a nice addition to the toolbox, or similar parsers
  - `clippy` seems to be the next best thing to start using
    - adding some vim plugins could help
  - cools things that people do:
    - use `criterion` or similar crate to benchmark solutions
    - try to solve all problems within 1 second
    - have a way to run all solutions at once
    - most of the older write-ups I have found mention the `z3` solver of particular problems accross the years. Seems like something nice to look.
  - the solution for `day03` probably could have leveraged some tree structure
  - the solution for `day05` is crazily overengineered, and could have been way simpler, but it was the first time that I have implemented operations for a structure and it was fun. Probably should have been done with macros. Something on the back of my mind tells me that it would be fun to implement some rules of linear algebra with the types and create some operations for vectors and scalars.
  - may refactor the first days for some consistency? document the journey? (working on it, days 01 and 02 done)
  - everytime I have a full grid, maybe I should use an mutable square array instead of maps indexed by (i,j)?
  - inputs from files are a nice way to consume puzzle input, but having parse_input receiving the filename hinders writing test cases or getting some inputs directly from the website
