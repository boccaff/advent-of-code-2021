use std::fs;

fn main() {
    let mut swarm: [isize; 9] = [0; 9];

    swarm[..].clone_from_slice(&parse_input("../data/day06_example.txt"));

    update_swarm(&mut swarm, 18, true);
    let s: isize = swarm.iter().sum();
    println!("Example at 18: {}", s);

    update_swarm(&mut swarm, 80 - 18, false);
    let s: isize = swarm.iter().sum();
    println!("Example at 80: {}", s);

    update_swarm(&mut swarm, 256 - 80, false);
    let s: isize = swarm.iter().sum();
    println!("Example at 256: {}", s);

    swarm[..].clone_from_slice(&parse_input("../data/day06_input.txt"));
    update_swarm(&mut swarm, 80, false);
    let s: isize = swarm.iter().sum();
    println!("Input after 80: {}", s);
    update_swarm(&mut swarm, 256 - 80, false);
    let s: isize = swarm.iter().sum();
    println!("Input after 256: {}", s);
}

fn parse_input(filename: &str) -> Vec<isize> {
    let mut v = vec![0; 9];
    for input in fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .split(",")
    {
        let i: usize = input.trim().parse().unwrap();
        v[i] += 1;
    }
    v
}

fn update_swarm(swarm: &mut [isize; 9], n: isize, print: bool) {
    for i in 0..n {
        swarm_step(swarm);
        if print {
            println!("{}: {:?}", i, swarm);
        }
    }
}

fn swarm_step(swarm: &mut [isize; 9]) {
    let a0 = swarm[0_usize];
    swarm.rotate_left(1);
    swarm[8_usize] = a0;
    swarm[6_usize] += a0;
}
