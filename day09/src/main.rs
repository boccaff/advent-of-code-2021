use day09::{parse_input, part1, part2};

fn main() {
    let input = parse_input("../data/day09_input.txt");
    let p1 = part1(&input);
    println!("Part1: {}", p1);

    let p2 = part2(&input);
    println!("Part2: {}", p2);
    //let p1 = part1(&input);
}
