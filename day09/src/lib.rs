use std::collections::{HashMap, HashSet};
use std::fs;

pub fn parse_input(filename: &str) -> HashMap<(i32, i32), i32> {
    let mut map = HashMap::new();

    for (i, line) in fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
        .enumerate()
    {
        for (j, elem) in parse_line(line).iter().enumerate() {
            map.insert((i as i32, j as i32), *elem);
        }
    }
    map
}

fn parse_line(line: &str) -> Vec<i32> {
    line.split_terminator("")
        .skip(1)
        .map(|x| {
            x.parse()
                .unwrap_or_else(|_| panic!("failed to parse ({}) as i32", x))
        })
        .collect()
}

fn neighbors(p: &(i32, i32), map: &HashMap<(i32, i32), i32>) -> Vec<(i32, i32)> {
    let (i, j) = *p;
    [(i, j + 1), (i, j - 1), (i + 1, j), (i - 1, j)]
        .iter()
        .filter(|(x, y)| map.contains_key(&(*x, *y)))
        .copied()
        .collect::<Vec<(i32, i32)>>()
}

fn basin_neighbor(p: &(i32, i32), map: &HashMap<(i32, i32), i32>) -> Vec<(i32, i32)> {
    let (i, j) = *p;
    [(i, j + 1), (i, j - 1), (i + 1, j), (i - 1, j)]
        .iter()
        .filter(|(x, y)| map.contains_key(&(*x, *y)) && map[&(*x, *y)] < 9)
        .copied()
        .collect::<Vec<(i32, i32)>>()
}

pub fn part1(input: &HashMap<(i32, i32), i32>) -> i32 {
    input
        .keys()
        .filter(|(i, j)| {
            neighbors(&(*i, *j), input)
                .iter()
                .all(|(x, y)| input[&(*i, *j)] < input[&(*x, *y)])
        })
        .map(|(i, j)| input[&(*i, *j)] + 1)
        .sum()
}

pub fn part2(input: &HashMap<(i32, i32), i32>) -> i32 {
    let mut basins: Vec<HashSet<(i32, i32)>> = Vec::new();
    let mut points = input.clone();

    let mut local_minima: Vec<(i32, i32)> = input
        .keys()
        .filter(|(i, j)| {
            neighbors(&(*i, *j), input)
                .iter()
                .all(|(x, y)| input[&(*i, *j)] < input[&(*x, *y)])
        })
        .copied()
        .collect();

    for lm in local_minima.iter() {
        points.remove(lm);
    }

    while local_minima.len() > 0_usize && points.len() > 0_usize {
        let p = local_minima.pop().unwrap();
        let mut nbrs = basin_neighbor(&p, input);

        let mut basin: HashSet<(i32, i32)> = HashSet::new();
        for n in nbrs.iter() {
            points.remove(n);
        }
        points.remove(&p);
        basin.insert(p);

        while nbrs.len() > 0_usize && points.len() > 0_usize {
            let n = nbrs.pop().unwrap();
            points.remove(&n);
            nbrs.extend_from_slice(&basin_neighbor(&n, &points));
            basin.insert(n);
        }
        basins.push(basin);
    }

    let mut lens = basins.iter().map(|x| x.len()).collect::<Vec<usize>>();
    lens.sort();
    lens.iter()
        .rev()
        .take(3)
        .fold(1, |acc, x| acc * (*x as i32))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let example = parse_input("../data/day09_example.txt");
        assert_eq!(part1(&example), 15);
    }

    #[test]
    fn test_part2() {
        let example = parse_input("../data/day09_example.txt");
        assert_eq!(part2(&example), 1134);
    }
}
