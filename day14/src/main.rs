use day14::{parse_input, part1, part2};

fn main() {
    let (mut template, rules) = parse_input("../data/day14_input.txt");
    let p1 = part1(&mut template, &rules);
    println!("Part 1: {}", p1);

    let (mut template, rules) = parse_input("../data/day14_input.txt");
    let p2 = part2(&mut template, &rules);
    println!("Part 2: {}", p2);
}
