use std::collections::HashMap;
use std::fs;

pub fn parse_input(filename: &str) -> (HashMap<(char, char), isize>, HashMap<(char, char), char>) {
    let mut template: HashMap<(char, char), isize> = HashMap::new();
    let mut rules: HashMap<(char, char), char> = HashMap::new();

    let input =
        fs::read_to_string(filename).unwrap_or_else(|_| panic!("Failed to read {}", filename));
    let (template_input, rules_input) = input
        .split_once("\n\n")
        .unwrap_or_else(|| panic!("Cannot split "));

    template_input
        .chars()
        .zip(template_input.chars().skip(1))
        .for_each(|key| {
            let counter = template.entry(key).or_insert(0);
            *counter += 1;
        });

    rules_input.lines().for_each(|line| {
        let (key, ins) = line.split_once(" -> ").unwrap();
        let mut key = key.chars();
        rules.insert(
            (key.next().unwrap(), key.next().unwrap()),
            ins.chars().next().unwrap(),
        );
    });
    (template, rules)
}

pub fn iterate_polymer(
    polymer: &HashMap<(char, char), isize>,
    rules: &HashMap<(char, char), char>,
) -> HashMap<(char, char), isize> {
    polymer.keys().fold(HashMap::new(), |mut acc, k| {
        if let Some(ins) = rules.get(k) {
            let occ = acc.entry((k.0, *ins)).or_insert(0);
            *occ += polymer[k];
            let occ = acc.entry((*ins, k.1)).or_insert(0);
            *occ += polymer[k];
            acc
        } else {
            acc.insert(*k, *polymer.get(k).unwrap());
            acc
        }
    })
}

pub fn count_letter_diff(
    polymer: &mut HashMap<(char, char), isize>,
    rules: &HashMap<(char, char), char>,
    n: isize,
) -> isize {
    for _ in 0..n {
        *polymer = iterate_polymer(polymer, rules);
    }

    let count = polymer
        .keys()
        .fold(HashMap::<char, isize>::new(), |mut acc, x| {
            let counter = acc.entry(x.1).or_insert(0);
            *counter += polymer[x];
            acc
        });

    count.values().max().unwrap() - count.values().min().unwrap()

}

pub fn part1(
    polymer: &mut HashMap<(char, char), isize>,
    rules: &HashMap<(char, char), char>,
) -> isize {
    count_letter_diff(polymer, rules, 10)
}

pub fn part2(
    polymer: &mut HashMap<(char, char), isize>,
    rules: &HashMap<(char, char), char>,
) -> isize {
    count_letter_diff(polymer, rules, 40)
}

#[cfg(test)]

mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let (mut template, rules) = parse_input("../data/day14_example.txt");
        let p1 = part1(&mut template, &rules);
        assert_eq!(p1, 1588)
    }

    #[test]
    fn test_part2() {
        let (mut template, rules) = parse_input("../data/day14_example.txt");
        let p2 = part2(&mut template, &rules);
        assert_eq!(p2, 2188189693529_isize)
    }

}
