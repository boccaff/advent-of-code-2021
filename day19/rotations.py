
def main():
    import numpy as np

    from itertools import combinations_with_replacement
    from functools import reduce

    rx = np.array([[ 1,  0,  0],
                   [ 0,  0, -1],
                   [ 0,  1,  0]
                ])

    ry = np.array([[ 0,  0,  1],
                   [ 0,  1,  0],
                   [-1,  0,  0]
                ])

    rz = np.array([[ 0, -1,  0],
                   [ 1,  0,  0],
                   [ 0,  0,  1]
                ])

    basis = np.array([1, 2, 3])

    I = np.array([[ 1,  0,  0],
                  [ 0,  1,  0],
                  [ 0,  0,  1]
                ])

    rotations = [rx, ry, rz]

    coords = []

    for i in range(1, 9):
        for ops in combinations_with_replacement(rotations, i):
            b = basis.copy()
            for r in ops:
                b = np.matmul(r, b)
            coords.append(tuple(b.tolist()))

    coords = list(set(coords))
    coords.sort()

    for i, coord in enumerate(coords):
        (x, y, z) = coord
        out = f"=> Self  {{x: {x}, y: {y}, z: {z}}},"
        out = out.replace("1", "x")
        out = out.replace("2", "y")
        out = out.replace("3", "z")
        print(f"{i} {out}")

if __name__ == "__main__":
    main()

