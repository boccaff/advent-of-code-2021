use day19::{parse_input, build_map, largest_distance};

fn main() {

    let input = parse_input("../data/day19_input.txt");
    let (p1, relative )= build_map(&input, 0);
    let p2 = largest_distance(&relative);
    println!("Part1: {}", p1.beacons.len());
    println!("Part2: {}", p2);

}
