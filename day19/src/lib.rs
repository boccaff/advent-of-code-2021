use std::collections::{HashMap, HashSet};
use std::ops::{Add, Sub};
use std::{fmt, fs};

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Point {
    x: i32,
    y: i32,
    z: i32,
}

impl Add for Point {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}, {}, {}", self.x, self.y, self.z)
    }
}

impl Point {
    pub fn new(x: i32, y: i32, z: i32) -> Self {
        Self { x, y, z }
    }

    pub fn rotate(&self, r: i32) -> Self {
        let Point { x, y, z } = *self;
        match r {
            0 => Self  {x: -z, y: -y, z: -x},
            1 => Self  {x: -z, y: -x, z: y},
            2 => Self  {x: -z, y: x, z: -y},
            3 => Self  {x: -z, y: y, z: x},
            4 => Self  {x: -y, y: -z, z: x},
            5 => Self  {x: -y, y: -x, z: -z},
            6 => Self  {x: -y, y: x, z: z},
            7 => Self  {x: -y, y: z, z: -x},
            8 => Self  {x: -x, y: -z, z: -y},
            9 => Self  {x: -x, y: -y, z: z},
            10 => Self  {x: -x, y: y, z: -z},
            11 => Self  {x: -x, y: z, z: y},
            12 => Self  {x: x, y: -z, z: y},
            13 => Self  {x: x, y: -y, z: -z},
            14 => Self  {x: x, y: y, z: z},
            15 => Self  {x: x, y: z, z: -y},
            16 => Self  {x: y, y: -z, z: -x},
            17 => Self  {x: y, y: -x, z: z},
            18 => Self  {x: y, y: x, z: -z},
            19 => Self  {x: y, y: z, z: x},
            20 => Self  {x: z, y: -y, z: x},
            21 => Self  {x: z, y: -x, z: -y},
            22 => Self  {x: z, y: x, z: y},
            23 => Self  {x: z, y: y, z: -x},
            _ => unreachable!(),
        }
    }

    pub fn manhattan(&self, other: &Point) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs() + (self.z - other.z).abs()
    }
}

#[derive(Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct Scanner {
    pub beacons: Vec<Point>,
}

impl Scanner {
    pub fn new(beacons: Vec<Point>) -> Self {
        Scanner { beacons }
    }

    pub fn from_vec_i32(beacons: &[Vec<i32>]) -> Self {
        Scanner {
            beacons: beacons
                .iter()
                .map(|v| Point::new(v[0_usize], v[1_usize], v[2_usize]))
                .collect(),
        }
    }
    pub fn rotate(&self, r: i32) -> Self {
        Scanner {
            beacons: self.beacons.iter().map(|b| b.rotate(r)).collect(),
        }
    }
}

impl Sub<&Point> for Scanner {
    type Output = Self;

    fn sub(self, other: &Point) -> Self {
        Scanner {
            beacons: self.beacons.clone().iter().map(|b| *b - *other).collect(),
        }
    }
}

impl Add<&Point> for Scanner {
    type Output = Self;

    fn add(self, other: &Point) -> Self {
        Scanner {
            beacons: self.beacons.clone().iter().map(|b| *b + *other).collect(),
        }
    }
}

impl fmt::Display for Scanner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let out: Vec<String> = self
            .beacons
            .iter()
            .map(|b| format!("({},{},{})", b.x, b.y, b.z))
            .collect();
        write!(f, "{}", out.join(", "))
    }
}

pub fn parse_input(filename: &str) -> HashMap<i32, Scanner> {
    let mut scanners: HashMap<i32, Scanner> = HashMap::new();

    fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Could not read {}", filename))
        .split("\n\n")
        .enumerate()
        .for_each(|(scanner, readings)| {
            let mut beacons: Vec<Vec<i32>> = Vec::new();
            readings.lines().for_each(|line| {
                if !line.starts_with("---") {
                    beacons.push(
                        line.split(",")
                            .map(|x| x.parse::<i32>().unwrap())
                            .collect::<Vec<i32>>(),
                    )
                }
            });
            let beacons = Scanner::from_vec_i32(&beacons);
            scanners.insert(scanner as i32, beacons);
        });
    scanners
}

pub fn match_scanners(s: &Scanner, t: &Scanner) -> Option<(i32, Point)> {
    for i in 0..s.beacons.len() {
        let ds = s.clone() - &s.beacons[i];
        for j in 0..t.beacons.len() {
            let dt = t.clone() - &t.beacons[j];
            for r in 0..24 {
                let dtr = dt.rotate(r);
                let intersection: Vec<Point> = dtr.beacons
                    .iter().filter(|x| ds.beacons.contains(x)).copied().collect();

                if intersection.len() >= 12 {

					let mut ms = s.beacons.iter()
						.zip(ds.beacons.iter())
                        .filter(|(_, dx)| intersection.contains(dx))
                        .map(|(x, dx)| (x.clone(), dx.clone()))
                        .collect::<Vec<(Point, Point)>>();
                    ms.sort_by_key(|x| x.1);

                    let mut mt = t.beacons.iter()
                        .zip(dtr.beacons.iter())
                        .filter(|(x, dx)| intersection.contains(dx))
                        .map(|(x, dx)| (x.rotate(r).clone(), dx.clone()))
                        .collect::<Vec<(Point, Point)>>();
                    mt.sort_by_key(|x| x.1);

                    let delta = ms.first().unwrap().0 - mt.first().unwrap().0;
                    return Some((r, delta));
                }
            }
        }
    }
    None
}

pub fn build_map(scanners: &HashMap<i32, Scanner>, basis: i32) -> (Scanner, HashMap<i32, Point>){

    let mut sol = scanners[&basis].clone();
    let mut relative: HashMap<i32, Point> = HashMap::new();

    let mut remaining = scanners.keys()
        .filter(|x| **x != basis)
        .copied()
        .collect::<Vec<i32>>();
    relative.insert(0, Point::new(0,0,0));

    'out: while remaining.len() > 0 {
        for (i, k) in remaining.iter().enumerate() {
            if let Some((r, delta)) = match_scanners(&sol, &scanners[k]) {
                let mut tmp = scanners[k].clone();
                tmp = tmp.rotate(r);
                tmp = tmp + &delta;
                tmp.beacons.sort();
                tmp.beacons.dedup();
                sol.beacons.extend(tmp.beacons.clone());
                relative.insert(*k, delta);
                remaining.remove(i);
                continue 'out;
            }
        }
        panic!("Failed to match remaining: {:?}", remaining)
    }
    sol.beacons.sort();
    sol.beacons.dedup();
    return (sol, relative)
}

pub fn largest_distance(relative: &HashMap<i32, Point>) -> i32 {
    let mut max = i32::MIN;

    for k0 in relative.keys() {
        for k1 in relative.keys() {
            let d = relative[k0].manhattan(&relative[k1]);
            max = max.max(d);
        }
    }
    max
}



#[cfg(test)]

mod tests {
    use super::*;

    #[test]
    fn test() {
        let input = parse_input("../data/day19_example_input.txt");
        let (p1, relative )= build_map(&input, 0);
        assert_eq!(p1.beacons.len(), 79);
        assert_eq!(largest_distance(&relative), 3621);
    }

}

