use std::fs;

fn hex_to_bin(input: &str) -> String {
    input
        .split_terminator("")
        .skip(1)
        .filter(|x| *x != "\n")
        .map(|x| {
            format!(
                "{:04b}",
                isize::from_str_radix(x, 16)
                    .unwrap_or_else(|_| panic!("Failed to parse digit: [{}]", x))
            )
        })
        .collect::<String>()
}

pub fn parse_input(filename: &str) -> String {
    hex_to_bin(
        &fs::read_to_string(filename)
            .unwrap_or_else(|_| panic!("Cannot read input for day16: {}", filename)),
    )
}

fn collect_literal(mut acc: String, input: &str) -> (String, &str) {
    if input[0_usize..1_usize] == "1".to_string() {
        acc.push_str(&input[1..5]);
        collect_literal(acc, &input[5..])
    } else {
        acc.push_str(&input[1..5]);
        (acc, &input[5..])
    }
}

fn parse_literal(input: &str) -> (isize, &str) {
    let (literal, remainder) = collect_literal("".to_owned(), input);
    (isize::from_str_radix(&literal, 2).unwrap(), remainder)
}

#[derive(Debug)]
enum Instruction {
    Literal(isize, isize),
    Operator(isize, isize, Vec<Instruction>),
}

fn parse_operator(input: &str) -> Option<(Vec<Instruction>, &str)> {
    let n_bits = if &input[..1_usize] == "1" { 11 } else { 15 };
    let l = isize::from_str_radix(&input[1..(n_bits + 1)], 2).unwrap();

    let mut res: Vec<Instruction> = Vec::new();
    let mut sub_input: &str;

    if n_bits == 15 {

        let offset = n_bits + (l as usize) + 2;
        sub_input = &input[(n_bits + 1)..(offset)];

        while sub_input.chars().any(|x| x != '0') && sub_input.len() > 7_usize {
            if let Some((r, remainder)) = parse_sub_package(sub_input) {
                sub_input = remainder;
                res.push(r)
            } else {
                unreachable!("Not able to parse: {:?}", input);
            }
        }

        sub_input = &input[(offset - sub_input.len())..];

    } else {

        sub_input = &input[(n_bits + 1)..];

        for _ in 0..l {
            if let Some((r, remainder)) = parse_sub_package(sub_input) {
                sub_input = remainder;
                res.push(r)
            } else {
                unreachable!()
            }
        }
    }

    Some((res, sub_input))
}

fn parse_sub_package(input: &str) -> Option<(Instruction, &str)> {
    if input.len() > 7_usize {
        let v = isize::from_str_radix(&input[..3], 2).unwrap();
        let t = isize::from_str_radix(&input[3..6], 2).unwrap();
        match t {
            4 => {
                let (x, remainder) = parse_literal(&input[6..]);
                Some((Instruction::Literal(v, x), remainder))
            }
            _ => {
                if let Some((x, remainder)) = parse_operator(&input[6..]) {
                    Some((Instruction::Operator(v, t, x), remainder))
                } else {
                    unreachable!("Failed to parse operator {:?}");
                }
            }
        }
    } else {
        None
    }
}

fn sum_version(i: &Instruction) -> isize {
    match i {
        Instruction::Literal(v, _) => *v,
        Instruction::Operator(v, _, ops) => *v + ops.iter().map(sum_version).sum::<isize>(),
    }
}

pub fn part1(input: &str) -> isize {
    let (ops, _) = parse_sub_package(input).unwrap();
    let p1 = sum_version(&ops);
    p1
}

fn operate(i: &Instruction) -> isize {
    match i {
        Instruction::Literal(_, x) => *x,
        Instruction::Operator(_, 0, ops) => ops.iter().fold(0_isize, |acc, x| acc + operate(x)), // sum
        Instruction::Operator(_, 1, ops) => ops.iter().fold(1_isize, |acc, x| acc * operate(x)), // product
        Instruction::Operator(_, 2, ops) => {
            ops.iter().fold(isize::MAX, |acc, x| acc.min(operate(x)))
        } // max
        Instruction::Operator(_, 3, ops) => {
            ops.iter().fold(isize::MIN, |acc, x| acc.max(operate(x)))
        } // min
        Instruction::Operator(_, 5, ops) => {
            if operate(&ops[0_usize]) > operate(&ops[1_usize]) {
                1
            } else {
                0
            }
        } // bigger
        Instruction::Operator(_, 6, ops) => {
            if operate(&ops[0_usize]) < operate(&ops[1_usize]) {
                1
            } else {
                0
            }
        } // bigger
        Instruction::Operator(_, 7, ops) => {
            if operate(&ops[0_usize]) == operate(&ops[1_usize]) {
                1
            } else {
                0
            }
        } // bigger
        _ => unreachable!(),
    }
}

pub fn part2(input: &str) -> isize {
    let (ops, _) = parse_sub_package(input).unwrap();
    //println!("{:?}", ops);
    let x = operate(&ops);
    x
}

#[cfg(test)]

mod tests {
    use super::*;

    #[test]
    fn test_op() {
        let (output, _) = parse_sub_package(&hex_to_bin("C200B40A82")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 3);

        let (output, _) = parse_sub_package(&hex_to_bin("04005AC33890")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 54);

        let (output, _) = parse_sub_package(&hex_to_bin("880086C3E88112")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 7);

        let (output, _) = parse_sub_package(&hex_to_bin("CE00C43D881120")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 9);

        let (output, _) = parse_sub_package(&hex_to_bin("D8005AC2A8F0")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 1);

        let (output, _) = parse_sub_package(&hex_to_bin("F600BC2D8F")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 0);

        let (output, _) = parse_sub_package(&hex_to_bin("9C005AC2F8F0")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 0);

        let (output, _) = parse_sub_package(&hex_to_bin("9C0141080250320F1802104A08")).unwrap();
        let x = operate(&output);
        assert_eq!(x, 1);
    }

    #[test]
    fn test_parse_sub_package_versions() {
        let (output, _) = parse_sub_package(&hex_to_bin("C0015000016115A2E0802F182340")).unwrap();
        let v = sum_version(&output);
        assert_eq!(v, 23);

        let (output, _) = parse_sub_package(&hex_to_bin("A0016C880162017C3686B18A3D4780")).unwrap();
        let v = sum_version(&output);
        assert_eq!(v, 31);

        let (output, _) = parse_sub_package(&hex_to_bin("620080001611562C8802118E34")).unwrap();
        let v = sum_version(&output);
        assert_eq!(v, 12);

        let (output, _) = parse_sub_package(&hex_to_bin("EE00D40C823060")).unwrap();
        let v = sum_version(&output);
        assert_eq!(v, 14);

        let (output, _) = parse_sub_package(&hex_to_bin("8A004A801A8002F478")).unwrap();
        let v = sum_version(&output);
        assert_eq!(v, 16);

        let (output, _) = parse_sub_package(&hex_to_bin("38006F45291200")).unwrap();
        let v = sum_version(&output);
        assert_eq!(v, 9);
    }

    #[test]
    fn test_parse_literal() {
        assert_eq!(parse_literal(&hex_to_bin("D2FE28")[6..]), (2021, "000"));
        assert_eq!(
            parse_literal("0101001010010001001000000000"),
            (10, "01010010001001000000000")
        );
        assert_eq!(parse_literal("10001001000000000"), (20, "0000000"));
    }

    #[test]
    fn test_parse_operator() {
        let (output, _) = parse_operator(&hex_to_bin("38006F45291200")[6..]).unwrap();

        if let Instruction::Literal(v0, x0) = output[0_usize] {
            assert_eq!(v0, 6);
            assert_eq!(x0, 10);
        } else {
            panic!()
        }

        if let Instruction::Literal(v1, x1) = output[1_usize] {
            assert_eq!(v1, 2);
            assert_eq!(x1, 20);
        } else {
            panic!()
        }

        let (output, _) = parse_operator(&hex_to_bin("EE00D40C823060")[6..]).unwrap();

        if let Instruction::Literal(v0, x0) = output[0_usize] {
            assert_eq!(v0, 2);
            assert_eq!(x0, 1);
        } else {
            panic!()
        }

        if let Instruction::Literal(v1, x1) = output[1_usize] {
            assert_eq!(v1, 4);
            assert_eq!(x1, 2);
        } else {
            panic!()
        }

        if let Instruction::Literal(v2, x2) = output[2_usize] {
            assert_eq!(v2, 1);
            assert_eq!(x2, 3);
        } else {
            panic!()
        }
    }

    #[test]
    fn test_bin() {
        let desired = "110100101111111000101000".to_string();
        assert_eq!(hex_to_bin("D2FE28"), desired);

        let desired = "00111000000000000110111101000101001010010001001000000000".to_string();
        assert_eq!(hex_to_bin("38006F45291200"), desired);

        let desired = "11101110000000001101010000001100100000100011000001100000".to_string();
        assert_eq!(hex_to_bin("EE00D40C823060"), desired);
    }
}

//005173980232D7F50C740109F3B9F3F0005425D36565F202012CAC0170004262EC658B0200FC3A8AB0EA5FF331201507003710004262243F8F600086C378B7152529CB4981400B202D04C00C0028048095070038C00B50028C00C50030805D3700240049210021C00810038400A400688C00C3003E605A4A19A62D3E741480261B00464C9E6A5DF3A455999C2430E0054FCBE7260084F4B37B2D60034325DE114B66A3A4012E4FFC62801069839983820061A60EE7526781E513C8050D00042E34C24898000844608F70E840198DD152262801D382460164D9BCE14CC20C179F17200812785261CE484E5D85801A59FDA64976DB504008665EB65E97C52DCAA82803B1264604D342040109E802B09E13CBC22B040154CBE53F8015796D8A4B6C50C01787B800974B413A5990400B8CA6008CE22D003992F9A2BCD421F2C9CA889802506B40159FEE0065C8A6FCF66004C695008E6F7D1693BDAEAD2993A9FEE790B62872001F54A0AC7F9B2C959535EFD4426E98CC864801029F0D935B3005E64CA8012F9AD9ACB84CC67BDBF7DF4A70086739D648BF396BFF603377389587C62211006470B68021895FCFBC249BCDF2C8200C1803D1F21DC273007E3A4148CA4008746F8630D840219B9B7C9DFFD2C9A8478CD3F9A4974401A99D65BA0BC716007FA7BFE8B6C933C8BD4A139005B1E00AC9760A73BA229A87520C017E007C679824EDC95B732C9FB04B007873BCCC94E789A18C8E399841627F6CF3C50A0174A6676199ABDA5F4F92E752E63C911ACC01793A6FB2B84D0020526FD26F6402334F935802200087C3D8DD0E0401A8CF0A23A100A0B294CCF671E00A0002110823D4231007A0D4198EC40181E802924D3272BE70BD3D4C8A100A613B6AFB7481668024200D4188C108C401D89716A080
