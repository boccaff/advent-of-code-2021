use std::collections::{HashMap, HashSet};
use std::fs;

const PAIRS: [(char, char); 8] = [
    ('(', ')'),
    ('[', ']'),
    ('{', '}'),
    ('<', '>'),
    (')', '('),
    (']', '['),
    ('}', '{'),
    ('>', '<'),
];

const OPENERS: [char; 4] = ['(', '{', '[', '<'];

fn parse_line(line: &str) -> (bool, Vec<char>) {
    let pairs: HashMap<char, char> = PAIRS.iter().copied().collect();
    let openers: HashSet<char> = OPENERS.iter().cloned().collect();

    let mut stack: Vec<char> = Vec::new();
    let mut ilegal: Vec<char> = Vec::new();
    let mut ok: bool = true;

    for elem in line.chars() {
        if openers.contains(&elem) {
            stack.push(elem);
        } else {
            let pair = pairs[&elem];
            if stack.ends_with(&[pair]) {
                stack.pop();
            } else {
                ok = false;
                ilegal.push(elem);
                return (ok, ilegal);
            }
        }
    }

    (ok, stack)
}

fn fix_points(incomplete: Vec<char>) -> isize {
    let pairs: HashMap<char, char> = PAIRS.iter().copied().collect();

    let fix_points: HashMap<char, isize> = [(')', 1), (']', 2), ('}', 3), ('>', 4)]
        .iter()
        .copied()
        .collect();

    let s: isize = incomplete
        .iter()
        .rev()
        .fold(0, |acc, x| (acc * 5) + fix_points[&pairs[&x]]);

    s
}

fn ilegal_points(ilegal: Vec<char>) -> isize {
    let ilegal_points: HashMap<char, isize> = [(')', 3), (']', 57), ('}', 1197), ('>', 25137)]
        .iter()
        .copied()
        .collect();

    ilegal_points[&ilegal.last().unwrap()]
}

pub fn parse_input(filename: &str) -> Vec<(bool, isize)> {
    fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
        .map(|line| {
            let (line_ok, maybe_ilegal) = parse_line(line);
            if line_ok {
                (line_ok, fix_points(maybe_ilegal))
            } else {
                (line_ok, ilegal_points(maybe_ilegal))
            }
        })
        .collect()
}

pub fn part1(input: &[(bool, isize)]) -> isize {
    input.iter().filter(|(b, x)| !*b).map(|(_, x)| x).sum()
}

pub fn part2(input: &[(bool, isize)]) -> isize {
    let mut res: Vec<isize> = input
        .iter()
        .filter(|(b, x)| *b && *x != 0)
        .map(|(_, x)| x)
        .copied()
        .collect();
    res.sort();
    res[res.len() / 2]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("../data/day10_example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 26397);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("../data/day10_example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 288957);
    }
}
