use day10::{parse_input, part1, part2};

fn main() {
    let input = parse_input(&"../data/day10_input.txt");
    let p1 = part1(&input);
    println!("Part 1: {}", p1);

    let p2 = part2(&input);
    println!("Part 2: {}", p2);
}
