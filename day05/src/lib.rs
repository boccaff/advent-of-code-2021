use std::fs;
use std::ops::{Add, Div, Mul, Sub};

pub trait AddMul: Add + Mul + Div + Sub + Copy + Clone + PartialEq {}

impl<T: Add + Mul + Div + Sub + Copy + Clone + PartialEq> AddMul for T {}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Point<T: AddMul> {
    pub x: T,
    pub y: T,
}

impl<T: AddMul> Point<T> {
    pub fn new(x: T, y: T) -> Self {
        Point { x, y }
    }
}

impl<T: AddMul + Add<Output = T>> Add for Point<T> {
    type Output = Self;
    fn add(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<'a, 'b, T: AddMul + Add<Output = T>> Add<&'b Point<T>> for &'a Point<T> {
    type Output = Point<T>;
    fn add(self, rhs: &'b Point<T>) -> Point<T> {
        Point::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl<T: AddMul + Sub<Output = T>> Sub for Point<T> {
    type Output = Self;
    fn sub(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl<'a, 'b, T: AddMul + Sub<Output = T>> Sub<&'b Point<T>> for &'a Point<T> {
    type Output = Point<T>;
    fn sub(self, rhs: &'b Point<T>) -> Point<T> {
        Point::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl<T: AddMul + Mul<Output = T>> Mul<T> for Point<T> {
    type Output = Self;
    fn mul(self, rhs: T) -> Self::Output {
        Self {
            x: (rhs * self.x),
            y: (rhs * self.y),
        }
    }
}

impl<T: AddMul + Div<Output = T>> Div<T> for Point<T> {
    type Output = Self;
    fn div(self, rhs: T) -> Self::Output {
        Self {
            x: (self.x / rhs),
            y: (self.y / rhs),
        }
    }
}

#[derive(Debug)]
pub struct Line<T>
where
    T: AddMul + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + Div<Output = T>,
{
    pub begin: Point<T>,
    pub end: Point<T>,
}

impl<T> Line<T>
where
    T: AddMul + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + Div<Output = T>,
{
    pub fn new(begin: (T, T), end: (T, T)) -> Self {
        let (x1, y1) = begin;
        let (x2, y2) = end;
        Line {
            begin: Point::new(x1, y1),
            end: Point::new(x2, y2),
        }
    }

    pub fn squared_len(&self) -> T {
        let delta = &self.end - &self.begin;
        let sqlen = delta.x * delta.x + delta.y * delta.y;
        sqlen
    }
}

pub fn parse_input(filename: &str) -> Vec<Line<i32>> {
    fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
        .map(|x| x.to_string())
        .map(|x| parse_line(&x))
        .collect()
}

fn parse_str_tuple(stuple: &str) -> (i32, i32) {
    let stuple = stuple
        .trim_start()
        .trim_end()
        .split(",")
        .map(|x| x.parse().unwrap())
        .collect::<Vec<i32>>();
    (stuple[0], stuple[1])
}

fn parse_line(line: &str) -> Line<i32> {
    let line = line.split("->").collect::<Vec<&str>>();
    let (begin, end) = (line[0], line[1]);
    let (begin, end) = (parse_str_tuple(end), parse_str_tuple(begin));

    Line::new(begin, end)
}

//let p1 = Point::new(5, 5);
//let p2 = Point::new(1, 1);

//println!("{:?}", p1 + p2);

//let p1 = Point::new(5, 5);
//println!("{:?}", &p1 + &p1);

//let p1 = Point::new(1.0, 1.0);
//println!("{:?}", p1 * 0.5);

//let p1 = Point::new(1, 1);
//println!("{:?}", p1 * 2);

//let p1 = Point::new(2, 2);
//println!("{:?}", p1 / 2);

//let p1 = Point::new(5, 5);
//println!("{:?}", &p1 - &p1);

//let line = Line::new((0, 0), (1, 1));
//println!("{}", (line.squared_len() as f64).sqrt());
