use std::env;

use day05::{parse_input, Point};

fn main() {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];

    let lines = parse_input(&filename);

    let mut v: Vec<Vec<Point<i32>>> = Vec::new();

    for line in lines.iter() {
        let l: i32 = (line.squared_len() as f64).sqrt() as i32 + 1_i32;
        let mut interp: Vec<Point<i32>> = Vec::new();

        let delta = line.end - line.begin;
        for i in 1..=l {
            let p = line.begin + (delta * i / l);
            interp.push(p)
        }
        interp.dedup();
        v.push(interp);
    }

    let maxx: i32 = lines
        .iter()
        .map(|x| {
            if x.begin.x < x.end.x {
                x.end.x
            } else {
                x.begin.x
            }
        })
        .max()
        .unwrap();

    let maxy: i32 = lines
        .iter()
        .map(|x| {
            if x.begin.y < x.end.x {
                x.end.y
            } else {
                x.begin.y
            }
        })
        .max()
        .unwrap();

    let mut floor: Vec<Vec<i32>> = vec![vec![0; (maxx + 1) as usize]; (maxy + 1) as usize];

    for (line, vi) in lines.iter().zip(v.iter()) {
        if (line.begin.x == line.end.x) || (line.begin.y == line.end.y) {
            for p in vi {
                let Point { x, y } = p;
                floor[*y as usize][*x as usize] += 1;
            }
        }
    }

    print_floor(&floor);

    let overlaps: i32 = floor
        .iter()
        .map(|x| x.iter().filter(|x| **x >= 2).copied().count() as i32)
        .sum();

    println!("Overlaping lines for part 1: {}", overlaps);

    let mut floor: Vec<Vec<i32>> = vec![vec![0; (maxx + 1) as usize]; (maxy + 1) as usize];

    for vi in v.iter() {
        for p in vi {
            let Point { x, y } = p;
            floor[*y as usize][*x as usize] += 1;
        }
    }

    print_floor(&floor);

    let overlaps: i32 = floor
        .iter()
        .map(|x| x.iter().filter(|x| **x >= 2).copied().count() as i32)
        .sum();
    println!("Overlaping lines for part 2: {}", overlaps);
}

fn print_floor(floor: &Vec<Vec<i32>>) {
    let width = floor[0].len() as i32;

    println!(
        "  {}",
        (0..width).map(|x| x.to_string()).collect::<String>()
    );
    for (i, row) in floor.iter().enumerate() {
        let out_row = row
            .iter()
            .map(|x| x.to_string())
            .map(|x| if x == "0" { ".".to_string() } else { x })
            .collect::<String>();
        println!("{}|{}", i, out_row);
    }
}
