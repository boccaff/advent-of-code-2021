use std::env;
use std::fs;

use std::collections::HashMap;

fn find_most_frequent_at_pos(map: &HashMap<&str, i32>) -> String {
    let total: i32 = map.values().sum::<i32>();
    let key_len: usize = map
        .keys()
        .map(|x| x.len())
        .fold(0_usize, |acc, x| if acc > x { acc } else { x });

    let mut ans = vec![0; key_len];

    for key in map.keys() {
        for i in 0_usize..key_len {
            if key.as_bytes()[i] == b'1' {
                ans[i] += 1
            }
        }
    }

    let ans: String = ans
        .iter()
        .map(|x| if *x > (total + 1) / 2 { '1' } else { '0' })
        .collect();
    ans
}

fn recursive_filtering(map: &HashMap<&str, i32>, most_frequent: bool) -> String {
    let key_len: usize = map
        .keys()
        .map(|x| x.len())
        .fold(0_usize, |acc, x| if acc > x { acc } else { x });

    let mut base = String::from("");
    let mut most: char = '1';
    let mut least: char = '0';

    if !most_frequent {
        most = '0';
        least = '1';
    }

    while base.len() < key_len {
        let mut count = 0;
        let mut sum = 0;

        let keys: Vec<&str> = map
            .keys()
            .filter(|x| x.starts_with(&base))
            .copied()
            .collect();

        if keys.len() == 1 {
            base = String::from(*keys.first().unwrap());
        } else {
            for key in map.keys() {
                if key.starts_with(&base) {
                    count += 1;
                    if key.as_bytes()[base.len()] == b'1' {
                        sum += 1;
                    }
                }
            }

            if sum >= (count + 1) / 2 {
                base.push(most);
            } else {
                base.push(least);
            }
        }
    }
    base
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];
    let mut map = HashMap::new();

    let contents: String =
        fs::read_to_string(filename).unwrap_or_else(|_| panic!("Cannot read file: {}", filename));

    for line in contents.lines() {
        let count = map.entry(line).or_insert(0);
        *count += 1;
    }

    let g: String = find_most_frequent_at_pos(&map);
    let eps: String = g
        .chars()
        .map(|x| if x == '1' { '0' } else { '1' })
        .collect();

    let g: i32 = i32::from_str_radix(&g, 2_u32).unwrap();
    let eps: i32 = i32::from_str_radix(&eps, 2_u32).unwrap();

    let o2: String = recursive_filtering(&map, true);
    let o2: i32 = i32::from_str_radix(&o2, 2_u32).unwrap();

    let co2: String = recursive_filtering(&map, false);
    let co2: i32 = i32::from_str_radix(&co2, 2_u32).unwrap();

    println!("\n\n\n-------------------");
    println!("Gamma:\t{}", &g);
    println!("Eps:\t{}", &eps);
    println!("Part 1: {}", g * eps);
    println!("Oxygen:\t{}", &o2);
    println!("CO2 scrubber: {}", &co2);
    println!("Part 2: {}", o2 * co2);
}
