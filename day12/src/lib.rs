use std::collections::{HashMap, HashSet};
use std::fs;

#[derive(Clone, Debug)]
struct CavePath {
    pub path: Vec<String>,
    pub caves: HashMap<String, i32>,
}

impl CavePath {
    pub fn new() -> Self {
        CavePath {
            path: Vec::<String>::new(),
            caves: HashMap::<String, i32>::new(),
        }
    }

    pub fn from_str(cave: &str) -> Self {
        let mut cp = CavePath::new();
        cp.push(cave);
        cp
    }

    pub fn push(&mut self, cave: &str) {
        self.path.push(cave.to_string());
        let counter = self.caves.entry(cave.to_string()).or_insert(0);
        *counter += 1;
    }

    pub fn last(&mut self) -> Option<&String> {
        self.path.last()
    }

    pub fn pop(&mut self) -> Option<String> {
        if let Some(cave) = self.path.pop() {
            self.caves.remove(&cave);
            Some(cave)
        } else {
            None
        }
    }

    pub fn contains(&self, cave: &str) -> bool {
        self.caves.contains_key(cave)
    }
}

fn is_small_cave(node: &str) -> bool {
    node.chars().all(|x| x.is_ascii_lowercase())
}

pub fn parse_input(filename: &str) -> HashMap<String, HashSet<String>> {
    let map: HashMap<String, HashSet<String>> = fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
        .fold(HashMap::new(), |mut acc, x| {
            let (beg, end) = x.split_once("-").unwrap();
            let counter = acc.entry(beg.to_string()).or_insert(HashSet::new());
            counter.insert(end.to_string());

            let counter = acc.entry(end.to_string()).or_insert(HashSet::new());
            counter.insert(beg.to_string());
            acc
        });
    map
}

fn filter_visit_once(cave: &str, path: &CavePath) -> bool {
    !path.contains(cave)
}

fn filter_part1(cave: &str, path: &CavePath) -> bool {
    !is_small_cave(cave) || !path.contains(cave)
}

fn filter_part2(cave: &str, path: &CavePath) -> bool {
    !is_small_cave(cave)
        || !path.contains(cave)
        || path
            .caves
            .keys()
            .filter(|x| is_small_cave(*x))
            .map(|x| path.caves[x])
            .fold(0, |acc, x| acc.max(x))
            <= 1
}

fn build_paths(
    from: &str,
    to: &str,
    map: &HashMap<String, HashSet<String>>,
    f: impl Fn(&str, &CavePath) -> bool,
) -> Vec<CavePath> {
    let mut paths: Vec<CavePath> = Vec::new();
    let mut candidates: Vec<CavePath> = vec![CavePath::from_str(from)];

    while let Some(mut candidate) = candidates.pop() {
        if let Some(last) = candidate.last() {
            if last == to {
                paths.push(candidate)
            } else {
                if let Some(possibilities) = map.get(last) {
                    for p in possibilities.iter() {
                        if p != from && f(p, &candidate) {
                            let mut new_candidate = candidate.clone();
                            new_candidate.push(&p);
                            candidates.push(new_candidate);
                        }
                    }
                }
            }
        }
    }

    paths
}

pub fn part1(input: &HashMap<String, HashSet<String>>) -> i32 {
    let paths = build_paths(&"start", &"end", input, filter_part1);
    paths.len() as i32
}

pub fn part2(input: &HashMap<String, HashSet<String>>) -> i32 {
    let paths = build_paths(&"start", &"end", input, filter_part2);
    paths.len() as i32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1p1() {
        let input = parse_input("../data/day12_example1.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 10);
    }

    #[test]
    fn example2p1() {
        let input = parse_input("../data/day12_example2.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 19);
    }

    #[test]
    fn example3p1() {
        let input = parse_input("../data/day12_example3.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 226);
    }

    #[test]
    fn example1p2() {
        let input = parse_input("../data/day12_example1.txt");
        println!("{:?}\n\n", input);
        let paths = build_paths(&"start", &"end", &input, filter_part2);
        for path in paths.iter() {
            println!("{}", path.path.join(","));
        }
        let p2 = part2(&input);
        assert_eq!(p2, 36);
    }

    #[test]
    fn example2p2() {
        let input = parse_input("../data/day12_example2.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 103);
    }

    #[test]
    fn example3p2() {
        let input = parse_input("../data/day12_example3.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 3509);
    }
}
