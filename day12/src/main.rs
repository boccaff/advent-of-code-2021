use day12::{parse_input, part1, part2};

fn main() {
    let input = parse_input("../data/day12_input.txt");
    let p1 = part1(&input);
    println!("Part 1: {}", p1);

    let input = parse_input("../data/day12_input.txt");
    let p2 = part2(&input);
    println!("Part 2: {}", p2);
}
