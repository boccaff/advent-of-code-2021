use day25::{parse_input, part1};

fn main() {
    let mut input = parse_input("../data/day25_input.txt");
    let p1 = part1(&mut input);
    println!("Part 1: {}", p1);
}
