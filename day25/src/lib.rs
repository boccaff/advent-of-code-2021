use std::collections::HashMap;
use std::fs;

#[derive(Debug, PartialEq, Eq)]
pub enum SeaCucumber {
    East,
    South,
}

type SeaFloor = HashMap<(i32, i32), Option<SeaCucumber>>;

// should be a From?
fn parse_char(c: char) -> Option<SeaCucumber> {
    match c {
        'v' => Some(SeaCucumber::South),
        '>' => Some(SeaCucumber::East),
        '.' => None,
        _ => panic!(
            "found unexpected '{}' while parsing sea floor map, should be ['>','v','.']",
            c
        ),
    }
}

pub fn parse_input(filename: &str) -> SeaFloor {
    let mut result: SeaFloor = HashMap::new();
    fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file {}", &filename))
        .lines()
        .enumerate()
        .for_each(|(i, line)| {
            line.chars().enumerate().for_each(|(j, c)| {
                result.insert((i as i32, j as i32), parse_char(c));
            })
        });
    result
}

fn circle_iter(k: i32, kmax: i32) -> i32 {
    if k < kmax {
        k+1
    } else {
        0
    }
}

fn herd_step(seafloor: &mut SeaFloor, imax: i32, jmax: i32) -> bool {
    let mut moved = false;

    for i in 0..=imax {
        let can_move: Vec<i32> = (0..=jmax)
            .filter(|j| seafloor[&(i, *j)] == Some(SeaCucumber::East) &&
                    seafloor[&(i, circle_iter(*j, jmax))].is_none())
            .map(|j| j)
            .collect();

        if can_move.len() > 0 {
            can_move.iter()
                .for_each(|j| {
                        seafloor.insert((i, *j), None);
                        seafloor.insert((i, circle_iter(*j, jmax)), Some(SeaCucumber::East));
            });
            moved = true;
        }

    }

    for j in 0..=jmax {
        let can_move: Vec<i32> = (0..=imax)
            .filter(|i| seafloor[&(*i, j)] == Some(SeaCucumber::South) &&
                    seafloor[&(circle_iter(*i, imax), j)].is_none())
            .map(|i| i)
            .collect();

        if can_move.len() > 0 {
            can_move.iter()
                .for_each(|i| {
                        seafloor.insert((*i, j), None);
                        seafloor.insert((circle_iter(*i, imax), j), Some(SeaCucumber::South));
            });
            moved = true;
        }

    }

    //for i in 0..=imax {
        //let mut j = 0;
        //while j <= jmax {
            //if seafloor[&(i,j)] == Some(SeaCucumber::East) {
                //let j_next = circle_iter(j, jmax);
                //if seafloor[&(i, j_next)].is_none() {
                    //seafloor.insert((i, j), None);
                    //seafloor.insert((i, j_next), Some(SeaCucumber::East));
                    //moved = true;
                    //j += 1;
                //}
            //}
            //j+=1;
        //}
    //}


    //for j in 0..=jmax {
        //let mut i = 0;
        //while i <= imax {
            //if seafloor[&(i,j)] == Some(SeaCucumber::South) {
                //let i_next = circle_iter(i, imax);
                //if seafloor[&(i_next, j)].is_none() {
                    //seafloor.insert((i, j), None);
                    //seafloor.insert((i_next, j), Some(SeaCucumber::South));
                    //moved = true;
                    //i += 1;
                //}
            //}
            //i += 1;
        //}
    //}

    moved

}

fn print_seafloor(seafloor: &SeaFloor) {
    let (imax, jmax) = seafloor.keys().fold((i32::MIN, i32::MIN), |acc, x| (acc.0.max(x.0), acc.1.max(x.1)));
    for i in 0..=imax {
        for j in 0..=jmax {
            match seafloor[&(i, j)] {
                Some(SeaCucumber::East) => print!(">"),
                Some(SeaCucumber::South) => print!("v"),
                None=> print!("."),
            }
        }
        print!("\n");
    }
    println!("");
}




pub fn part1(seafloor: &mut SeaFloor) -> i32 {
    let mut i = 0;
    let (imax, jmax) = seafloor.keys()
        .fold((i32::MIN, i32::MIN), |acc, x| (acc.0.max(x.0), acc.1.max(x.1)));

    while herd_step(seafloor, imax, jmax) {
        i+=1;
    }
    i + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_circle_iter() {
        assert_eq!(circle_iter(0, 9), 1);
        assert_eq!(circle_iter(9, 9), 0);
    }

    #[test]
    fn test_part1() {
        let mut example = parse_input("../data/day25_example.txt");
        let p1 = part1(&mut example);
        assert_eq!(p1, 58)
    }

}
