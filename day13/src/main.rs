use day13::{parse_input, print_coords, transform};

fn main() {
    let (coords, folds) = parse_input("../data/day13_input.txt");

    let p1 = transform(&coords, &folds[..1]).len();
    println!("Part 1: {}", p1);

    let p2 = transform(&coords, &folds);
    print_coords(&p2);
    //println!("Part 1: {}", p1);
}
