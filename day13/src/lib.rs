use std::fs;

#[derive(Debug)]
pub enum Direction {
    X,
    Y,
}

#[derive(Debug)]
pub struct Fold {
    pub direction: Direction,
    pub value: i32,
}

impl Fold {
    pub fn from_str(instruction: &str) -> Self {
        let (direction, value) = instruction.split_once("=").unwrap();
        let value = value.parse().unwrap();
        if direction.contains("x") {
            Fold {
                direction: Direction::X,
                value,
            }
        } else {
            Fold {
                direction: Direction::Y,
                value,
            }
        }
    }
}

fn fold_y(coords: &Vec<(i32, i32)>, yfold: i32) -> Vec<(i32, i32)> {
    coords
        .iter()
        .map(|(x, y)| {
            if y > &yfold {
                (*x, 2 * yfold - y)
            } else {
                (*x, *y)
            }
        })
        .clone()
        .collect()
}

fn fold_x(coords: &Vec<(i32, i32)>, xfold: i32) -> Vec<(i32, i32)> {
    coords
        .iter()
        .map(|(x, y)| {
            if x > &xfold {
                (2 * xfold - x, *y)
            } else {
                (*x, *y)
            }
        })
        .clone()
        .collect()
}

pub fn parse_input(filename: &str) -> (Vec<(i32, i32)>, Vec<Fold>) {
    let input =
        fs::read_to_string(filename).unwrap_or_else(|_| panic!("Cannot read file: {}", filename));
    let (coords, folds): (&str, &str) = input.split_once("\n\n").unwrap();

    (
        coords
            .lines()
            .map(|x| x.split_once(",").unwrap())
            .map(|(x, y)| (x.parse().unwrap(), y.parse().unwrap()))
            .collect::<Vec<(i32, i32)>>(),
        folds
            .lines()
            .map(|x| Fold::from_str(x))
            .collect::<Vec<Fold>>(),
    )
}

pub fn folder(coords: &[(i32, i32)], folds: &[Fold]) -> Vec<(i32, i32)> {
    let mut transformed: Vec<(i32, i32)> = coords.to_vec();

    //println!("{}", folds.len());

    //println!("In: {:?}\n", &transformed);
    for fold in folds.iter() {
        match fold {
            Fold {
                direction: Direction::X,
                value: x,
            } => {
                transformed = fold_x(&transformed, *x);
            }
            Fold {
                direction: Direction::Y,
                value: y,
            } => {
                transformed = fold_y(&transformed, *y);
            }
        }
        //println!("it: {:?}\n", &transformed);
    }
    transformed
}

pub fn print_coords(coords: &[(i32, i32)]) {
    let (ymax, xmax) = coords
        .iter()
        .fold((0, 0), |acc, x| (acc.0.max(x.0), acc.1.max(x.1)));
    for i in 0..=xmax {
        for j in 0..=ymax {
            if coords.contains(&(j, i)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        print!("\n");
    }
}

pub fn transform(coords: &[(i32, i32)], folds: &[Fold]) -> Vec<(i32, i32)> {
    let mut transformed = folder(coords, &folds);
    transformed.sort();
    transformed.dedup();
    transformed
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1_1() {
        let (coords, folds) = parse_input("../data/day13_example.txt");
        assert_eq!(transform(&coords, &folds[..1]).len(), 17);
    }

    #[test]
    fn test_part1_2() {
        let (coords, folds) = parse_input("../data/day13_example.txt");
        assert_eq!(transform(&coords, &folds[..2]).len(), 16);
    }
}
