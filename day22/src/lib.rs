use std::fs;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Instruction {
    is_on: bool,
    start: [i32; 3],
    end: [i32; 3],
}

impl Instruction {
    fn new(is_on: bool, start: &[i32], end: &[i32]) -> Self {
        Instruction {
            is_on: is_on,
            start: [start[0], start[1], start[2]],
            end: [end[0], end[1], end[2]],
        }
    }

    fn volume(&self) -> isize {
        let vol = self.end
            .iter()
            .zip(self.start.iter())
            .map(|(e, s)| ((e - s).abs() + 1) as isize)
            .product();
        if self.is_on {
            vol
        } else {
            -vol
        }
    }

    fn contains(&self, other: &Instruction) -> bool {
        (0..3).all(|i| other.start[i] >= self.start[i] && other.end[i] <= self.end[i])
    }

    fn intersect(&self, other: &Instruction) -> bool {
        (0..3).all(|i| {
            !(self.start[i] > other.end[i] || self.end[i] < other.start[i])
        })
    }

    fn intersection(&self, other: &Instruction) -> Option<Self> {
        if self.intersect(other) {
            Some(Instruction::new(
                is_intersection_on(self.is_on, other.is_on),
                &self
                    .start
                    .iter()
                    .zip(other.start.iter())
                    .map(|(i, j)| *i.max(j))
                    .collect::<Vec<i32>>(),
                &self
                    .end
                    .iter()
                    .zip(other.end.iter())
                    .map(|(i, j)| *i.min(j))
                    .collect::<Vec<i32>>(),
            ))
        } else {
            None
        }
    }
}

fn is_intersection_on(left: bool, right: bool) -> bool {
    match (left, right) {
        (true, true) => false,
        (true, false) => false,
        (false, true) => true,
        (false, false) => true,
    }
}

impl std::fmt::Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "{} x={}..{},y={}..{},z={}..{} [{}]",
                    if self.is_on {"on"} else {"off"},
                    self.start[0], self.end[0],
                    self.start[1], self.end[1],
                    self.start[2], self.end[2],
                    self.volume(),
                    )
    }
}

fn print_cube(cube: &Cube, level: &str) {
    println!("{}{}", level, cube.cube);
    if cube.nodes.len() > 0 {
        for lilcube in cube.nodes.iter() {
            print_cube(lilcube, &("  ".to_owned() + level))
        }
    }
}

fn parse_range(strange: &str) -> (i32, i32) {
    let (_, numbers) = strange.split_once('=').unwrap();
    let (ll, ul) = numbers.split_once("..").unwrap();
    (ll.parse::<i32>().unwrap(), ul.parse::<i32>().unwrap())
}

fn parse_line(line: &str) -> Instruction {
    let (set, rest) = line.split_once(' ').unwrap();
    let ranges: Vec<(i32, i32)> = rest.split(',').map(|x| parse_range(x)).collect();
    Instruction::new(
        set == "on",
        &ranges.iter().map(|x| x.0).collect::<Vec<i32>>(),
        &ranges.iter().map(|x| x.1).collect::<Vec<i32>>(),
    )
}

pub fn parse_input(filename: &str) -> Vec<Instruction> {
    fs::read_to_string(&filename)
        .unwrap()
        .lines()
        .map(|line| parse_line(line))
        .collect::<Vec<Instruction>>()
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Cube {
    cube: Instruction,
    nodes: Vec<Cube>,
}

impl Cube {
    fn new(instruction: &Instruction) -> Self {
        Cube {
            cube: instruction.clone(),
            nodes: Vec::new(),
        }
    }

    fn apply(&mut self, inst: &Instruction) {
        if let Some(intersection) = self.cube.intersection(inst) {
            self.nodes.iter_mut().for_each(|cube| cube.apply(inst));
            self.nodes.push(Cube::new(&intersection));
        }
    }


}

fn build_reactor(instructions: &[Instruction]) -> Vec<Cube> {
    let mut reactor = vec![Cube::new(instructions.first().unwrap())];

    for inst in instructions.iter().skip(1) {
        reactor.iter_mut().for_each(|cube| cube.apply(inst));
        if inst.is_on {
            reactor.push(Cube::new(&inst))
        }
    }

    reactor

}

fn cube_volume(cube: &Cube) -> isize{
    let v = cube.cube.volume();
    let sv = cube.nodes.iter().map(|x| cube_volume(x)).sum::<isize>();
    v + sv
}

fn sum_volume(reactor: &[Cube]) -> isize {
    reactor.iter()
        .map(|x| cube_volume(x))
        .sum::<isize>()
}

pub fn part1(input: &[Instruction]) -> isize {
    let bounds = Instruction {
        is_on: true,
        start: [-50; 3],
        end: [50; 3],
    };
    let initialization: Vec<Instruction> = input
        .iter()
        .filter(|inst| bounds.contains(inst))
        .map(|x| *x)
        .collect();
    let reactor = build_reactor(&initialization);
    sum_volume(&reactor)
}

pub fn part2(input: &[Instruction]) -> isize {
    let reactor = build_reactor(input);
    sum_volume(&reactor)
}


#[cfg(test)]

mod tests {
    use super::*;

    #[test]
    fn test_part2() {
        let input = parse_input("../data/day22_example2.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 2758514936282235)
    }

    #[test]
    fn test_part1() {
        let input = parse_input("../data/day22_example1.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 590784)
    }

    #[test]
    fn test_toy() {
        let input = parse_input("../data/day22_toy.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 39)
    }

}
