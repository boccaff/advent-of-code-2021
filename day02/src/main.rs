use day02::{parse_input, part1, part2};

fn main() {
    let input = parse_input("../data/day02_input.txt");
    let p1 = part1(&input);
    let p2 = part2(&input);

    println!("Part 1:\n {}", p1);
    println!("Part 2:\n {}", p2);
}
