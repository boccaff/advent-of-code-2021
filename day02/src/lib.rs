use std::fs;

struct Submarine {
    x: i32,
    y: i32,
}

impl Submarine {
    fn new() -> Self {
        Submarine { x: 0, y: 0 }
    }

    fn vertical(&mut self, change: i32) {
        self.y += change;
    }

    fn horizontal(&mut self, change: i32) {
        self.x += change;
    }

    fn position(&self) -> i32 {
        self.x * self.y
    }
}

struct AimSubmarine {
    x: i32,
    y: i32,
    aim: i32,
}

impl AimSubmarine {
    fn new() -> Self {
        AimSubmarine { x: 0, y: 0, aim: 0 }
    }

    fn up(&mut self, change: i32) {
        self.aim -= change;
    }

    fn down(&mut self, change: i32) {
        self.aim += change;
    }

    fn forward(&mut self, change: i32) {
        self.x += change;
        self.y += change * self.aim;
    }

    fn position(&self) -> i32 {
        self.x * self.y
    }
}

pub fn parse_navigation(instruction: &str) -> (String, i32) {
    let parsed: Vec<&str> = instruction.split_whitespace().collect();
    let direction: String = String::from(parsed[0_usize]);
    let change: i32 = parsed[1_usize].parse().expect("Cannot parse change");
    (direction, change)
}


pub fn parse_input(filename: &str) -> Vec<(String, i32)> {
    fs::read_to_string(filename)
        .expect("Cannot read file")
        .lines()
        .map(|x| parse_navigation(x))
        .collect()
}

pub fn part1(instruction: &[(String, i32)]) -> i32 {
    let mut sub = Submarine::new();

    for (direction, change) in instruction.iter() {
        match (direction.as_str(), change) {
            ("forward", x) => {
                sub.horizontal(*x);
            }
            ("down", x) => {
                sub.vertical(*x);
            }
            ("up", x) => {
                sub.vertical(-*x);
            }
            (_, _) => panic!("Direction is not in [up, down, forward]: {}", direction),
        }
    }
    sub.position()
}

// how can I create a type that group Submarine and AimSubmarin?
// that would allow the creation of a function run_directions
// where it can receive a generic submarine type and run the
// list of instructions.
// This would allow the deduplication of code between part 1 and 2

pub fn part2(instruction: &[(String, i32)]) -> i32 {

    let mut sub = AimSubmarine::new();

    for (direction, change) in instruction.iter() {
        match (direction.as_str(), change) {
            ("forward", x) => {
                sub.forward(*x);
            }
            ("down", x) => {
                sub.down(*x);
            }
            ("up", x) => {
                sub.up(*x);
            }
            (_, _) => panic!("Direction is not in [up, down, forward]: {}", direction),
        }
    }

    sub.position()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("../data/day02_example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 150);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("../data/day02_example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 900);
    }

}

