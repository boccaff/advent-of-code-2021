use std::fs;

pub fn parse_input(filename: &str) -> Vec<i32> {
    fs::read_to_string(filename)
        .expect("Cannot read file")
        .lines()
        .map(|line| line.parse::<i32>().unwrap())
        .collect()
}

pub fn part1(input: &[i32]) -> i32 {
    input.windows(2)
        .filter(|x| x[1_usize] > x[0_usize])
        .count() as i32
}

pub fn part2(input: &[i32]) -> i32 {
    input.windows(3)
        .filter(|x| x[2_usize] > x[0_usize])
        .count() as i32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("../data/day01_example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 7);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("../data/day01_example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 5);
    }
}
