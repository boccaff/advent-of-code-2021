use std::fs;
use std::collections::{HashMap, BinaryHeap};
use std::cmp::Ordering;

#[derive(Clone, Debug, Copy)]
pub struct Node{
    pub x: i32,
    pub y: i32,
    pub cost: i32,
}

// Straight from Rust Doc, where this is implemented for a Djikstra example
// The priority queue depends on `Ord`.
// Explicitly implement the trait so the queue becomes a min-heap
// instead of a max-heap.
impl Ord for Node{
    fn cmp(&self, other: &Self) -> Ordering {
        // As the doc, comparison is flipped. Usual is self.cost.cmp(other.cost)
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
		other.cost.cmp(&self.cost)
			.then_with(|| self.cost.cmp(&other.cost))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for Node{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialEq for Node{
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl Eq for Node {}

impl Node {
	fn new(x: i32, y: i32, cost: i32) -> Self {
		Node {x, y, cost}
	}
}


pub fn parse_input(filename: &str) -> HashMap<(i32, i32), i32> {
	let mut map: HashMap<(i32, i32), i32> = HashMap::new();

    fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
		.enumerate()
        .for_each(|(i, line)| {
            line.split_terminator("")
                .skip(1)
				.enumerate()
                .for_each(|(j, x)|
					{map.insert((i as i32, j as i32), x.parse().unwrap());}
					)
        });

	map
}

fn neighbors4(pos: &(i32, i32), ll: &(i32, i32), ul: &(i32, i32)) -> Vec<(i32, i32)> {
    [(0, -1), (0, 1), (1, 0), (-1, 0)]
        .iter()
        .map(|(di, dj)| (pos.0 + di, pos.1 + dj))
        .filter(|(i, j)| i >= &ll.0 && j >= &ll.1)
        .filter(|(i, j)| i <= &ul.0 && j <= &ul.1)
        .collect()
}

fn calc_cost(path: &[(i32, i32)], grid: &HashMap<(i32, i32), i32>) -> i32 {
    path.iter().map(|point| grid.get(point).unwrap()).sum()
}

fn reconstruct_path(
    to: &(i32, i32),
    came_from: &HashMap<(i32, i32), (i32, i32)>,
) -> Vec<(i32, i32)> {
    let mut path: Vec<(i32, i32)> = Vec::new();
    let mut current = to;

    while came_from.contains_key(current) {
        path.push(*current);
        current = &came_from[current];
    }

    path.reverse();
    path
}

fn find_path(
    grid: &HashMap<(i32, i32), i32>,
) -> Vec<(i32, i32)> {

    let from = (0, 0);
	let to = grid.keys().fold((0, 0), |acc, p| (acc.0.max(p.0), acc.1.max(p.1)));

    let mut open_set: BinaryHeap<Node> = BinaryHeap::new();
    let mut came_from: HashMap::<(i32, i32), (i32, i32)> = HashMap::new();
    let mut g_score : HashMap::<(i32, i32), i32> = HashMap::new();

	open_set.push(Node::new(0, 0, 0));
    g_score.insert(from, 0);

    while let Some(p) = open_set.pop() {
        if (p.x, p.y) == to {
            // this is not necessary, as p already store the accumulated cost
            // return could be just p.cost, but performace impact is negligible
            // and having the full path is a better solution.
            return reconstruct_path(&(p.x, p.y), &came_from)
        } else {
            for ngb in neighbors4(&(p.x, p.y), &from, &to).iter() {
                let tentative = g_score[&(p.x, p.y)] + grid[ngb];
                let g_neighbor = g_score.entry(*ngb).or_insert(i32::MAX);
                if tentative < *g_neighbor {
                    came_from.insert(*ngb, (p.x, p.y));
                    *g_neighbor = tentative;
                    open_set.push(Node::new(ngb.0, ngb.1, tentative));
                }
            }
        }
    }
    panic!("Failed to find solution!")
}


pub fn part1(grid: &HashMap<(i32, i32), i32>) -> i32 {
    let path = find_path(grid);
    let cost = calc_cost(&path, &grid);
    cost
}

fn make_input2(grid: &HashMap<(i32, i32), i32>) -> HashMap<(i32, i32), i32> {
    let mut out: HashMap<(i32, i32), i32> = HashMap::new();
	let (mut m, mut n) = grid.keys().fold((0, 0), |acc, p| (acc.0.max(p.0), acc.1.max(p.1)));

    m += 1;
    n += 1;

    for i in 0..(5 * m) {
        for j in 0..(5 * n) {
            let x = i % m;
            let k = i / m;
            let y = j % n;
            let l = j / n;

            let mut cost: i32 = *grid.get(&(x, y)).unwrap();

            if k == 0 && l ==  0 {
				out.insert((i, j), cost);
			} else {
                cost = cost + k + l;
                cost = if cost > 9 { cost % 9} else {cost};
				out.insert((i, j), cost);
            };
        }
    }
    out
}


pub fn part2(grid: &HashMap<(i32, i32), i32>) -> i32 {
    let grid = make_input2(grid);
    let path = find_path(&grid);
    let cost = calc_cost(&path, &grid);

    cost
}


#[cfg(test)]

mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let grid = parse_input("../data/day15_example.txt");
        let p1 = part1(&grid);
        assert_eq!(p1, 40);
    }

    #[test]
    fn test_part2() {
        let grid = parse_input("../data/day15_example.txt");
        let p2 = part2(&grid);
        assert_eq!(p2, 315);
    }

}
