use day15::{parse_input, part1, part2};

fn main() {
    let grid = parse_input("../data/day15_input.txt");
    let p1 = part1(&grid);
    println!("Part 1: {}", p1);

    let p2 = part2(&grid);
    println!("Part 2: {}", p2);
}
