use day17::{parse_input, run_simulations, part1, part2};

fn main() {
    let target = parse_input("../data/day17_input.txt");
    let simulations = run_simulations(&target);

    let p1 = part1(&simulations);
    let p2 = part2(&simulations);

    println!("{:?}", p1);
    println!("{:?}", p2);
}
