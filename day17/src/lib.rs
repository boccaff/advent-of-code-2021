use std::{fs, fmt};
use core::ops::{Add, Sub, RangeInclusive};

#[derive(Debug, Clone, Copy)]
pub struct Point {
    pub x: isize,
    pub y: isize,
}

impl Point {
    fn new(x: isize, y: isize) -> Self {
        Point {x, y}
    }
}

impl PartialEq for Point {

    fn eq(&self, other: &Point) -> bool {
        self.x == other.x && self.y == other.y
    }

}

impl Eq for Point {}

impl Add for Point {

    type Output = Self;

    fn add(self, other: Point) -> Self {
        Self {x: self.x + other.x, y: self.y + other.y }
    }

}

impl Sub for Point {

    type Output = Self;

    fn sub(self, other: Point) -> Self {
        Self {x: self.x - other.x, y: self.y - other.y }
    }

}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug)]
pub struct Target {
    pub bl: Point,
    pub tr: Point,
}

impl Target {

    pub fn range_x(&self) -> RangeInclusive<isize> {
        self.bl.x..=self.tr.x
    }

    pub fn range_y(&self) -> RangeInclusive<isize> {
        self.bl.y..=self.tr.y
    }

}

#[derive(Debug)]
pub struct Probe {
    pos: Point,
    v: Point,
}

impl Probe {

    pub fn past(&self, target: &Target) -> bool {
        self.pos.y < target.bl.y && self.v.y < 0
    }

    pub fn within(&self, target: &Target) -> bool {
        target.range_x().contains(&self.pos.x) && target.range_y().contains(&self.pos.y)
    }

    pub fn increment_pos(&mut self) {
        self.pos = self.pos + self.v;
    }

    pub fn update_speed(&mut self, a: Point) {
        self.v = self.v + a;
    }

}


fn parse_point_input(input: &str) -> (isize, isize) {
    let pair = input.split_once("..").unwrap();
    (pair.0.parse::<isize>().unwrap(), pair.1.parse::<isize>().unwrap())
}

pub fn parse_input(filename: &str) -> Target {
    let line = fs::read_to_string(filename).unwrap_or_else(|_| panic!("Failed to read: {}", filename)).replace("target area: ", "").replace("\n", "");

    let mut target: (&str, &str) = line.split_once(", ").unwrap();
    target = (target.0.split_once("=").unwrap().1, target.1.split_once("=").unwrap().1);
    let bounds: ((isize, isize), (isize, isize)) = (parse_point_input(target.0), parse_point_input(target.1));

    Target {
        bl: Point {x: bounds.0.0, y: bounds.1.0},
        tr: Point {x: bounds.0.1, y: bounds.1.1}
    }
}


fn shoot_probe(v0: &Point, target: &Target) -> Result<Vec<Point>, &'static str> {
    let mut probe = Probe {pos: Point {x: 0, y: 0}, v: v0.clone()};
    let mut out: Vec<Point> = vec![probe.pos.clone()];
    let mut jackpot = false;

    while !probe.past(target) {
        probe.increment_pos();
        out.push(probe.pos.clone());
        probe.update_speed(Point::new(-probe.v.x.signum(), -1));
        if probe.within(&target) {
            jackpot = true;
        }
    }

    if jackpot {
        Ok(out)
    } else {
        Err("Missed target!")
    }

}

pub fn run_simulations(target: &Target) -> Vec<Vec<Point>> {

    let mut sims: Vec<Vec<Point>> = Vec::new();
    for vx in 1..=(target.tr.x) {
        for vy in (target.bl.y)..=(-target.bl.y){
            if let Ok(res) = shoot_probe(&Point::new(vx, vy), target) {
                sims.push(res);
            }
        }
    }

    sims.sort_by_key(|s| s.iter().map(|pos| pos.y).max().unwrap());
    sims.dedup();
    sims
}

pub fn part1(sims: &[Vec<Point>]) -> isize {
    sims.last().unwrap().iter().map(|pos| pos.y).max().unwrap()
}

pub fn part2(sims: &[Vec<Point>]) -> isize {
    sims.len() as isize
}

