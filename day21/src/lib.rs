use std::fs;

pub fn parse_input(filename: &str) -> (i32, i32) {
    let players: Vec<i32> = fs::read_to_string(filename)
        .unwrap()
        .lines()
        .map(|line| {
            line.split_once(":")
                .unwrap()
                .1
                .trim_start()
                .parse()
                .unwrap()
        })
        .collect();
    (players[0], players[1])
}

fn deterministic_dice() -> impl Iterator<Item = i32> {
    (1..=100).into_iter()
}

fn score(pos: i32, moves: i32) -> i32 {
    let pos0 = (pos + (moves % 10)) % 10;
    if pos0 == 0 {
        10
    } else {
        pos0
    }
}

pub fn part1(tp: &(i32, i32)) -> i32 {
    let (mut p1, mut p2) = tp;
    let mut sp1 = 0;
    let mut sp2 = 0;

    let mut dice = deterministic_dice();
    let mut count = 0;

    loop {
        let mut move_p1 = 0;
        for _ in 0..3 {
            if let Some(v) = dice.next() {
                move_p1 += v;
                count += 1;
            } else {
                dice = deterministic_dice();
                let v = dice.next().unwrap();
                move_p1 += v;
                count += 1;
            }
        }

        p1 = score(p1, move_p1);
        sp1 += p1;
        if sp1 >= 1000 {
            break;
        };

        let mut move_p2 = 0;
        for _ in 0..3 {
            if let Some(v) = dice.next() {
                move_p2 += v;
                count += 1;
            } else {
                dice = deterministic_dice();
                let v = dice.next().unwrap();
                move_p2 += v;
                count += 1;
            }
        }
        p2 = score(p2, move_p2);
        sp2 += p2;
        if sp2 >= 1000 {
            break;
        };
    }
    sp1.min(sp2) * count
}

const BRANCHES: [(i32, i32); 7] = [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)];

pub fn part2(tp: &(i32, i32)) -> usize {
    let mut games: Vec<[usize; 6]> = Vec::new();
    let mut sp1: usize = 0;
    let mut sp2: usize = 0;

    games.push([1_usize, tp.0 as usize, 0_usize, tp.1 as usize, 0_usize, 1_usize]);

    while games.len() > 0 {
        if let Some(game) = games.pop() {
            if game[2] >= 21 {
                sp1 += game[5];
            } else if game[4] >= 21 {
                sp2 += game[5];
            } else {
                if game[0] == 1 {
                    BRANCHES.iter()
                        .for_each(|(d, w)| {
                            let mut cp = game.clone();
                            cp[0] = 0_usize;
                            cp[1] = score(cp[1] as i32, *d) as usize;
                            cp[2] += cp[1];
                            cp[5] *= *w as usize;
                            games.push(cp)
                        });
                } else {
                    BRANCHES.iter()
                        .for_each(|(d, w)| {
                            let mut cp = game.clone();
                            cp[0] = 1_usize;
                            cp[3] = score(cp[3] as i32, *d) as usize;
                            cp[4] += cp[3];
                            cp[5] *= *w as usize;
                            games.push(cp)
                        });
                }
            }
        }
    }
    sp1.max(sp2)
}



#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("../data/day21_example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 739785)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("../data/day21_example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 444356092776315)
    }
}
