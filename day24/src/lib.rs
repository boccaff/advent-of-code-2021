#[derive(Debug, Copy, Clone)]
enum Store {
    Raw(isize),
    Stored(usize),
}

#[derive(Debug, Copy, Clone)]
enum Instruction {
    Inp(Store),
    Add(Store, Store),
    Mul(Store, Store),
    Div(Store, Store),
    Mod(Store, Store),
    Eql(Store, Store),
}

impl From<&str> for Store {
    fn from(item: &str) -> Self {
        match item {
            "w" => Store::Stored(0_usize),
            "x" => Store::Stored(1_usize),
            "y" => Store::Stored(2_usize),
            "z" => Store::Stored(3_usize),
            _ => Store::Raw(
                item.parse()
                    .unwrap_or_else(|_| panic!("Tried to convert [{}] into Store.", item)),
            ),
        }
    }
}

impl From<&str> for Instruction {
    fn from(item: &str) -> Self {
        let mut vals = item.split(" ");
        match vals.next() {
            Some("inp") => Instruction::Inp(vals.next().unwrap().into()),
            Some("add") => {
                Instruction::Add(vals.next().unwrap().into(), vals.next().unwrap().into())
            }
            Some("mul") => {
                Instruction::Mul(vals.next().unwrap().into(), vals.next().unwrap().into())
            }
            Some("div") => {
                Instruction::Div(vals.next().unwrap().into(), vals.next().unwrap().into())
            }
            Some("mod") => {
                Instruction::Mod(vals.next().unwrap().into(), vals.next().unwrap().into())
            }
            Some("eql") => {
                Instruction::Eql(vals.next().unwrap().into(), vals.next().unwrap().into())
            }
            _ => panic!("Invalid instruction received: {}", item),
        }
    }
}

impl Instruction {
    fn process<'a>(self, storage: &mut [isize; 4], input: &mut Vec<i32>) {
        match self {
            Instruction::Inp(Store::Stored(p)) => storage[p] = input.pop().unwrap() as isize,

            Instruction::Add(Store::Stored(p), Store::Raw(v)) => storage[p] += v as isize,
            Instruction::Add(Store::Stored(p0), Store::Stored(p1)) => storage[p0] += storage[p1],

            Instruction::Mul(Store::Stored(p), Store::Raw(v)) => storage[p] *= v as isize,
            Instruction::Mul(Store::Stored(p0), Store::Stored(p1)) => storage[p0] *= storage[p1],

            Instruction::Div(Store::Stored(p), Store::Raw(v)) => storage[p] /= v as isize,
            Instruction::Div(Store::Stored(p0), Store::Stored(p1)) => storage[p0] /= storage[p1],

            Instruction::Mod(Store::Stored(p), Store::Raw(v)) => storage[p] %= v as isize,
            Instruction::Mod(Store::Stored(p0), Store::Stored(p1)) => storage[p0] %= storage[p1],

            Instruction::Eql(Store::Stored(p), Store::Raw(v)) => {
                if storage[p] == v as isize {
                    storage[p] = 1
                } else {
                    storage[p] = 0
                }
            }
            Instruction::Eql(Store::Stored(p0), Store::Stored(p1)) => {
                if storage[p0] == storage[p1] {
                    storage[p0] = 1
                } else {
                    storage[p0] = 0
                }
            }
            _ => panic!("Instruction {:?} cannot be processed", self),
        }
    }
}

fn parse_input(input: &str) -> Vec<Instruction> {
    input.lines().map(|line| line.into()).collect()
}

fn alu(instructions: &[Instruction], input: &[i32]) -> [isize; 4] {

    let mut storage: [isize; 4] = [0; 4];
    let mut input: Vec<i32> = input.to_vec().iter().map(|x| *x).rev().collect();

    instructions.iter()
        .for_each(|inst| {
                  inst.process(&mut storage, &mut input);
                //println!("{:?}\t{:?}", storage, inst);
        }
                  );

    storage
}

pub fn gen_alu(program: &str, input: &[i32]) -> [isize; 4] {
    let instructions = parse_input(program);
    alu(&instructions, input)
}

pub fn str_to_digits(input: &str) -> Vec<i32> {
    input.bytes().map(|c| (c - b'0') as i32).collect()
}


pub fn part1(input: &str) -> usize {
    let instructions = parse_input(input);
    let mut v: usize = 99999999999999;
    loop {
        let alu_input = str_to_digits(&v.to_string());
        if alu_input.contains(&0) {
            v -= 1;
            //if v % 100000 == 0 {
                //println!("{}", v);
            //}
            continue
        } else {
            //println!("{} entered", v);
            let ans = alu(&instructions, &alu_input);
            //println!("{}, {:?}", v, ans);
            if ans[3_usize] == 0 {
                break
            } else {
                v -= 1
            }
        }
    }
    v
}


#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test1() {
        let input = "inp x
mul x -1
";
        let instructions = parse_input(input);
        let inp = vec![1];
        let r = alu(&instructions, &inp);
        assert_eq!(r[1_usize], -1)
    }

    #[test]
    fn test2() {
    let input = "inp z
inp x
mul z 3
eql z x";
        let instructions = parse_input(input);
        let inp = vec![1, 3];
        let r = alu(&instructions, &inp);
        assert_eq!(r[3_usize], 1);

        let inp = vec![1, 4];
        let r = alu(&instructions, &inp);
        assert_eq!(r[3_usize], 0);
    }

    #[test]
    fn test3() {
        let input = "inp w
add z w
mod z 2
div w 2
add y w
mod y 2
div w 2
add x w
mod x 2
div w 2
mod w 2";

        let instructions = parse_input(input);
        let inp = vec![9];
        let r = alu(&instructions, &inp);
        assert_eq!(r, [1, 0, 0, 1]);

        let inp = vec![5];
        let r = alu(&instructions, &inp);
        assert_eq!(r, [0, 1, 0, 1]);
    }

}
