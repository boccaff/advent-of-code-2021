use std::fs;
use day24::{str_to_digits, gen_alu};

fn main() {

    let program = fs::read_to_string("../data/day24_input.txt").unwrap();
    let digits_max = str_to_digits("12996997829399");
    let digits_min = str_to_digits("11841231117189");

    let alu_max = gen_alu(&program, &digits_max);
    println!("Max: {:?}", alu_max);

    let alu_min = gen_alu(&program, &digits_min);
    println!("Min: {:?}", alu_min);
}

