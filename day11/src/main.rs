use day11::{iterate_map, parse_input, part1, part2, print_map};
use std::io::{self, Write};
use std::{thread, time};

fn main() {
    let mut input = parse_input("../data/day11_input.txt");
    let p1 = part1(&mut input, 100);
    println!("Part 1: {}", p1);

    let mut input = parse_input("../data/day11_input.txt");
    let p2 = part2(&mut input);

    println!("Part 2: {}", p2);

    loop {
        let mut input = parse_input("../data/day11_example.txt");
        for i in 0..=195 {
            println!("{}:", i);
            print_map(&input);
            iterate_map(&mut input);
            io::stdout().flush().unwrap();
            print!("\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A");
            thread::sleep(time::Duration::from_millis(100));
        }
        thread::sleep(time::Duration::from_millis(1000));
    }
}
