use std::collections::HashMap;
use std::fs;

extern crate colored;
use colored::*;

pub fn parse_input(filename: &str) -> HashMap<(i32, i32), i32> {
    let mut map = HashMap::new();

    for (i, line) in fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
        .enumerate()
    {
        for (j, elem) in parse_line(line).iter().enumerate() {
            map.insert((i as i32, j as i32), *elem);
        }
    }
    map
}

fn parse_line(line: &str) -> Vec<i32> {
    line.split_terminator("")
        .skip(1)
        .map(|x| {
            x.parse()
                .unwrap_or_else(|_| panic!("failed to parse ({}) as i32", x))
        })
        .collect()
}

fn neighbors(p: &(i32, i32), map: &HashMap<(i32, i32), i32>) -> Vec<(i32, i32)> {
    let (i, j) = *p;
    [
        (i + 1, j + 1),
        (i + 1, j - 1),
        (i + 1, j),
        (i - 1, j + 1),
        (i - 1, j - 1),
        (i - 1, j),
        (i, j + 1),
        (i, j - 1),
    ]
    .iter()
    .filter(|(x, y)| map.contains_key(&(*x, *y)))
    .copied()
    .collect::<Vec<(i32, i32)>>()
}

pub fn print_map(map: &HashMap<(i32, i32), i32>) {
    let (m, n): (i32, i32) = map
        .keys()
        .fold((0, 0), |acc, (x, y)| (acc.0.max(*x), acc.1.max(*y)));
    println!("");
    for i in 0..=m {
        for j in 0..=n {
            let v = map[&(i, j)];
            match v {
                0 => print!("{}", v.to_string().bold()),
                9 => print!("{}", v.to_string().red().bold()),
                _ => print!("{}", v.to_string().bright_white()),
            }
        }
        print!("\n");
    }
}

fn update_point(p: &(i32, i32), e: &(i32, i32), map: &mut HashMap<(i32, i32), i32>) {
    if let Some(x) = map.get_mut(p) {
        *x += 1;
        if *x == 10 {
            for n in neighbors(p, map).iter() {
                if n != e {
                    update_point(&n, p, map);
                }
            }
        }
    }
}

pub fn iterate_map(map: &mut HashMap<(i32, i32), i32>) -> i32 {
    let (m, n): (i32, i32) = map
        .keys()
        .fold((0, 0), |acc, (x, y)| (acc.0.max(*x), acc.1.max(*y)));

    let mut s: i32 = 0;

    for i in 0..=m {
        for j in 0..=n {
            update_point(&(i, j), &(-1, -1), map);
        }
    }

    for i in 0..=m {
        for j in 0..=n {
            if let Some(x) = map.get_mut(&(i, j)) {
                if *x > 9 {
                    s += 1;
                    *x = 0
                }
            }
        }
    }

    s
}

pub fn part1(map: &mut HashMap<(i32, i32), i32>, n: i32) -> i32 {
    let mut res: i32 = 0;

    for _ in 0..n {
        res += iterate_map(map);
    }
    res
}

pub fn part2(map: &mut HashMap<(i32, i32), i32>) -> i32 {
    let mut res: i32 = 0;
    let mut i: i32 = 0;

    while res != 100 {
        res = iterate_map(map);
        i += 1;
    }
    i
}

#[cfg(test)]
mod tests {
    use super::*;

    //#[test]
    //fn test_toy() {
    //let mut example = parse_input("../data/day11_toy.txt");
    //assert_eq!(part1(&mut example, 2), 9);
    //}

    #[test]
    fn test_part1() {
        let mut example = parse_input("../data/day11_example.txt");
        assert_eq!(part1(&mut example, 100), 1656);
    }

    #[test]
    fn test_part2() {
        let mut example = parse_input("../data/day11_example.txt");
        assert_eq!(part2(&mut example), 195);
    }
}
