use std::env;
use std::fs;

struct Bingo {
    columns: Vec<i32>,
    rows: Vec<i32>,
    board: Vec<Vec<i32>>,
    matches: Vec<i32>,
}

impl Bingo {
    fn new(board: Vec<Vec<i32>>) -> Self {
        Self {
            columns: vec![0; board[0].len()],
            rows: vec![0; board.len()],
            board,
            matches: Vec::new(),
        }
    }

    fn update(&mut self, drawn: &i32) {
        for (i, line) in self.board.iter().enumerate() {
            for (j, e) in line.iter().enumerate() {
                if *e == *drawn {
                    self.rows[i as usize] += 1;
                    self.columns[j as usize] += 1;
                    self.matches.push(*drawn);
                }
            }
        }
    }

    fn check_for_win(&self, drawn: &i32) -> i32 {
        let l = self.board[0].len() as i32;
        if self.rows.contains(&l) || self.columns.contains(&l) {
            (self
                .board
                .iter()
                .map(|x| x.iter().sum::<i32>())
                .sum::<i32>()
                - self.matches.iter().sum::<i32>())
                * drawn
        } else {
            0
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];

    let (draw, boards) = parse_input(&filename);

    let mut boards: Vec<Bingo> = boards
        .iter()
        .map(|board| Bingo::new(board.to_vec()))
        .collect();

    let mut winners: Vec<usize> = Vec::new();
    let mut points: Vec<i32> = Vec::new();

    while winners.len() < boards.len() {
        for (d, drawn) in draw.iter().enumerate() {
            for (b, board) in boards.iter_mut().enumerate() {
                board.update(drawn);
                let w = board.check_for_win(drawn);
                if w > 0 {
                    if !winners.contains(&b) {
                        println!(
                            "{} draw: {}, board {} with {} points",
                            d + 1_usize,
                            drawn,
                            b + 1_usize,
                            w
                        );
                        winners.push(b);
                        points.push(w);
                    }
                }
            }
        }
    }
    println!(
        "{}, {}",
        winners.first().unwrap() + 1,
        points.first().unwrap()
    );
    println!(
        "{}, {}",
        winners.last().unwrap() + 1,
        points.last().unwrap()
    );
}

fn parse_input(filename: &str) -> (Vec<i32>, Vec<Vec<Vec<i32>>>) {
    let mut contents: Vec<String> = Vec::new();
    let mut boards: Vec<Vec<Vec<i32>>> = Vec::new();
    let mut board: Vec<Vec<i32>> = Vec::new();

    for line in fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read file: {}", filename))
        .lines()
    {
        contents.push(line.to_string());
    }

    let drawn: Vec<i32> = contents
        .first()
        .unwrap()
        .clone()
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect::<Vec<i32>>()
        .to_vec();

    for line in contents.iter().skip(2) {
        if &line == &"" {
            boards.push(board.clone());
            board = Vec::new();
        } else {
            board.push(
                line.split_whitespace()
                    .map(|x| x.parse().unwrap())
                    .collect::<Vec<i32>>()
                    .to_vec(),
            );
        }
    }

    boards.push(board);

    (drawn, boards)
}
